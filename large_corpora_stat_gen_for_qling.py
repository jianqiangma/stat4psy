#-*- coding: utf-8 -*-

# The general idea of the code design
# =============================================================
#
#
#
#
# Two dimenssions of statistics  type-token and unit grain
# -----------------------------------------------------------------
#
# **type-token**
#
# * two kindes of abstract stat operation: token_collect, type_collect
#
# - token_collect:
# for a type or a set of type, how many tokens have occurred in a unit? Usually implemented as **dictionary**:
# key=type_to_be_considered,  value=count
#
# - type_collect:
#
# how many distinct types have appeared? usually keep a **set**
#
# examples of token_collect
# --freq of a word/character in a sentence
# --document or the whole corpus
#
# exmaples of type collect:
# --num of distince word type that contain
#
#
# **unit grain**
#
# * sentence
# * paragraph
# * document
# * corpus/multi-corpus
#
# Note that certain corpora (Gigaword, SogouT) are not sentence-split. So as a quick start-up, we only implement *sentence*, *document*, and *corpus* level
#
#
#
#
# Statistics being considered until now
# -------------------------------------------------
# 1. character frequency,
# 2. contextual diversity (similar to doc frequency?),
# 3. num_of-time-in-distint-word-type (word-segmentation required) 
#
# Two versions of corpus: character-based and word-based
# ---------------------------------------------------------------
#
# usually use word-based, as character-based can be induced
#
# General purpose algorithm
# ====================================================
#
# >consider two-levels, *sent* and *doc* in this subroutine
#
# def function_to_process_an_doc():
#
#     initialize sent_token_stat_dict, sent_type_stat_set
#
#     for line in doc:
#         sent_token_collection(line, token,stat)
#         sent_type_collection(line, type,stat)
#
#     doc_type_stat_set = set(sent_token_stat_dict.keys())
#
#     return sent_token_stat_dict, sent_type_stat_set, doc_type_stat_set
#
#
# def compute_frequency_counts(path_to_corpus):
#
#      docs=[]
#
#      for current_line in file:
#
#          line_buffer.append(current_line.strip())
#
#          if current_line is end_of_doc:  # the stuff in line buffer is a document
#              docs.append(line_buffer)
#              line_buffer=[]
#
#              if batch_line_counter >x and current line is a doc_end or y line being read:
#                  # process current docs
#
#                  list_s_token, list_s_type, list_doc_type = pool.map(function_to_process_a_doc, docs)
#                  pool.close()
#                  pool.join()
#
#                  global_update(list_s_token, list_s_type, list_doc_type)
#
#          else:
#               batch_line_counter += 1
#
#
#
#

import cPickle as pickle
import codecs, multiprocessing, sys, time, cProfile, os.path

#
# function to deal with a single document, same as in [6]
#
def gen_stat_for_a_doc(char_set_n_doc):

    char_set, doc = char_set_n_doc
    char2freq, char2wordSet = dict(), dict()

    char3gram2freq = dict() # collect char trigram freq


    #char_freq={char:0 for char in char_set}
    #char2word={char:set() for char in char_set}

    word_count, two_char_word_count, char_count, target_two_char_count = 0, 0, 0, 0
    #char3gram_count = 0


    target_word2freq={}

    for line_paragraph in doc:
        #print line

        # de-facto sentence split
        line_list = line_paragraph.replace(u"。",u"\n").replace(u"！",u"\n").replace(u"？", u"\n").split('\n')

        for line in line_list:

            word_seq=line.split()

            # char trigram statistics collection
            char_seq=u"#"+u"".join(word_seq)+u"#"  # here '#' indicates sent boundary
            for char_index in range(1, len(char_seq)-1 ):
                char3gram = u"".join(char_seq[char_index-1:char_index+2])

                if char3gram[1] in char_set:    # only cares those tri-gram the the central char is in the target char set
                    char3gram2freq[char3gram] = char3gram2freq.get(char3gram, 0) + 1
                    #char3gram_count += 1


            for word in word_seq:
                #print 'word=', word

                word_count += 1
                char_count += len(word)

                flag = False
                for char in word:
                    if char in char_set:
                        flag = True

                        char2freq[char] = char2freq.get(char, 0) + 1
                        char2wordSet.setdefault(char, set()).add(word)
                        #print char, char2freq[char], u''.join(char2wordSet[char])

                if len(word)==2:  #the word contains target character(s), thus becomes a target word for us
                    two_char_word_count += 1

                if flag:
                    target_word2freq[word] = target_word2freq.get(word, 0) + 1

                    if len(word) == 2:
                        target_two_char_count += 1  #check-sum

    return char2freq, char2wordSet, target_word2freq, char_count, word_count, two_char_word_count, target_two_char_count\
        , char3gram2freq #, char3gram_count


def update_corpus_level_stat(result_list, char2freq_corpus, char2wordSet_corpus, char2docCount_corpus,\
                             word2freq_corpus, total_char, total_word, total_2char_word, char3gram2freq_corpus):

    print '\tUpdating global statistics'

    for char2freq, char2wordSet, target_word2freq, char_count, word_count, two_char_word_count, target_two_char_count, char3gram2freq in result_list:
        for char in char2freq:
            char2freq_corpus[char] += char2freq[char]
            char2wordSet_corpus[char].update(char2wordSet[char])
            char2docCount_corpus[char] += 1

        for word in target_word2freq:
            word2freq_corpus[word] =  word2freq_corpus.get(word, 0) + target_word2freq[word]


        total_char += char_count
        total_word += word_count
        total_2char_word += two_char_word_count

        for char3gram in char3gram2freq:
            char3gram2freq_corpus [char3gram] = char3gram2freq_corpus.get(char3gram, 0) + char3gram2freq[char3gram]



    result_list=[]

    return total_char, total_word, total_2char_word


#test_multiprocessing of stat collection
def compute_frequency_counts(path_to_corpus, char_set, end_of_doc_sign, num_process, batch_size):

    start_time = time.time()
    second_before_backup = 43200  # backup every 12 hours

    pickle_intermediate_result = False

    char2freq_corpus={char:0 for char in char_set}
    char2wordSet_corpus={char:set() for char in char_set}
    char2docCount_corpus={char:0 for char in char_set}

    char3gram2freq_corpus = {}

    word2freq_corpus={}

    total_char, total_word, total_2char_word = 0,  0, 0

    line_count, cum_line_count, batch_count = 0, 0, 0

    line_buffer=[]
    docs=[]

    with codecs.open(path_to_corpus,'rU', 'utf-8') as f:
        print '\nReading segmented corpus from', path_to_corpus, '...'

        for line in f:

            line_count += 1
            line_buffer.append(line)

            #
            #triger doc collection once end_of_doc sign is seen
            #
            if line.split() and len(line)>20 and ''.join(line.split())==end_of_doc_sign:

                docs.append(line_buffer)
                line_buffer=[]


                #
                # batch process multiple documents, when there are large enough docs cummulated
                #
                if line_count > batch_size: #simulate creteria for staring multi-processing

                    cum_line_count += line_count
                    line_count = 0
                    batch_count += 1

                    print '\n', cum_line_count, 'lines has been read.'
                    charset_doc_tuples=[(char_set, doc) for doc in docs]
                    docs=[]

                    print 'Now batch process, running multiprocessing...'
                    pool = multiprocessing.Pool(num_process)
                    #print 'Fake multiprocessing going on...'

                    try:
                        result_list = pool.map_async(gen_stat_for_a_doc, charset_doc_tuples).get(9999999)
                        #purpose of using map_async and a timeout parameter ===> to make keyboard interuption feasible,
                        #see http://stackoverflow.com/questions/1408356/keyboard-interrupts-with-pythons-multiprocessing-pool

                    except KeyboardInterrupt:
                        pool.terminate()
                        print "You cancelled the program!"
                        sys.exit(1)

                    pool.close()
                    pool.join()


                    #
                    #update result to corpus_level statistics
                    #
                    total_char, total_word, total_2char_word = update_corpus_level_stat(result_list, char2freq_corpus, char2wordSet_corpus, char2docCount_corpus,\
                                             word2freq_corpus, total_char, total_word, total_2char_word, char3gram2freq_corpus)
                    result_list=[]

                    if time.time()-start_time > second_before_backup:


                        print '\nPickleing intermediate results...'
                        pickle_path=path_to_corpus+'.tmp_result.'+str(time.time())+'.pickle'
                        with open(pickle_path, 'wb') as f:
                            pickle.dump((char2freq_corpus,char2wordSet_corpus,char2docCount_corpus), f)

                        start_time = time.time()


            # NOT a end_of_doc line, i.e. ordinary lines
            #else:
                #pass

    if line_buffer:
        print '(diagnostic info:left-over lines)'
        docs.append(line_buffer)

    if docs:
        print '(diagnostic info: left-over docs will be processed, num of such docs=',len(docs),')'

        charset_doc_tuples=[(char_set, doc) for doc in docs]

        print '\nBatch process, running multiprocessing...\n'


        pool=multiprocessing.Pool(num_process)
        result_list=pool.map(gen_stat_for_a_doc, charset_doc_tuples)
        pool.close()
        pool.join()

        total_char, total_word, total_2char_wor = update_corpus_level_stat(result_list, char2freq_corpus, char2wordSet_corpus,\
                                                                           char2docCount_corpus,word2freq_corpus, total_char, total_word, total_2char_word, char3gram2freq_corpus)
        result_list = []

    print '\n>>done!'
    #return char2freq_corpus, char2wordSet_corpus, char2docCount_corpus, word2freq_corpus, total_char, total_word, total_2char_word, char3gram2freq_corpus
    return char2freq_corpus, char2wordSet_corpus,  char2docCount_corpus, word2freq_corpus, char3gram2freq_corpus


#
# new function to write the result...
#
def pickle_statistics(char2freq_corpus, char2wordSet_corpus, char2docCount_corpus, word2freq_corpus, char3gram2freq_corpus, path_to_corpus, path_to_csv):

    mid_affix = "."+os.path.basename(path_to_csv)

    output1, output2, output3, output4, output5 = path_to_corpus+mid_affix+'.char2freq.output.pickle',\
                                         path_to_corpus+mid_affix+'.char2wordSet.output.pickle',\
                                         path_to_corpus+mid_affix+'.char2docCount.output.pickle',\
                                         path_to_corpus+mid_affix+'.word2freq.output.pickle',\
                                         path_to_corpus+mid_affix+'.char3gram2freq.output.pickle'

    print '\n\n>> Writing results for current corpus to files', output1, output2, output3, output4, output5

    with open(output1, 'wb') as f:
        pickle.dump(char2freq_corpus, f)

    with open(output2, 'wb') as f:
        pickle.dump(char2wordSet_corpus, f)

    with open(output3, 'wb') as f:
        pickle.dump(char2docCount_corpus, f)

    with open(output4, 'wb') as f:
        pickle.dump(word2freq_corpus, f)

    with open(output5, 'wb') as f:
        pickle.dump(char3gram2freq_corpus, f)



def compute_probability(item2freq, char_set):


    total_freq=float(sum(item2freq.values()))
    item2prob={ item:item2freq[item]/total_freq for item in item2freq}



    for char in char_set:
        if not char in item2prob:
            item2prob[char] = 'NA'


    return item2prob


def add_one_smooth_freq(item2freq, char_set):

    char2count = {}

    for char in char_set:
        if char in item2freq:
            char2count[char] = item2freq[char] + 1

        else:
            char2count[char] = 1


    return char2count



#
# (out-of-date!) function for compute probability, no longer used, as we want to output frequency count (with smoothing) directly
#
if False:

    def compute_prob(char2freq_corpus, char2wordSet_corpus, char2docCount_corpus, char_set):
        print '\n\nCompute char probability, doc_occurrence prob for char, uniq_word_count_prob for char'

        char_prob = compute_probability(char2freq_corpus, char_set)

        char2word_count = {char: len(char2wordSet_corpus[char]) for char in char2wordSet_corpus}
        char_word_prob = compute_probability(char2word_count, char_set)

        char_doc_prob = compute_probability(char2docCount_corpus, char_set)

        return  char_prob,  char_word_prob, char_doc_prob


#
# actually return the raw freq count instead of normalize things into probability
#
def compute_smoothed_freq(char2freq_corpus, char2wordSet_corpus, char2docCount_corpus, char_set):


    char_prob = add_one_smooth_freq(char2freq_corpus, char_set)

    char2word_count = {char: len(char2wordSet_corpus[char]) for char in char2wordSet_corpus}
    char_word_prob = add_one_smooth_freq(char2word_count, char_set)

    char_doc_prob = add_one_smooth_freq(char2docCount_corpus, char_set)

    return char_prob,  char_word_prob, char_doc_prob  #these are smoothed frequency rather than real probabilities


#
# (out-of-data) we don't read char info from reaction time data anymore. Now we use read_char_list () to read characters
#               from a seperite list
#
#def read_cvs(char_cvs):
#    with codecs.open(char_cvs, 'rU', 'utf-8') as f:
#
#        if '2500' in char_cvs:
#            char_info_list = [line.strip().split(',') for line in f.readlines()]
#            head_str=char_info_list[0]
#            char_info_list = char_info_list[1:]
#        elif '1273' in char_cvs:
#            char_info_list = [line.strip().split(',')[1:] for line in f.readlines()]
#            head_str = char_info_list[0]
#            char_info_list = char_info_list
#
#        else:
#            print '\Error! The input_data is not the recognized file!'
#
#
#    char_set = set([i[0] for i in char_info_list])
#
#    print '\n# of char to be considered =', len(char_set)
#
#    return char_info_list, head_str, char_set



def read_char_list(char_path):

    with codecs.open(char_path, 'ru', 'utf-8') as f:
        char_set =set([token for line in f for token in line.split()])
        print '\nsize of character size=', len(char_set)
        print 'example characters:', ' '.join(list(char_set)[:min(3, len(char_set))])

    return char_set






def main(char_cvs,path_to_corpus,end_of_doc_sign, pickle_flag):

    batch_size = 32000
    num_process = min(24, multiprocessing.cpu_count())

    x=time.time()

    #read cvs file that contains reaction time data
    #char_list, header, char_set = read_cvs(char_cvs)
    char_set = read_char_list(char_cvs)  # read a space delimited character list instead, does not consider reaction time data

    # collect the frequency count from the large corpus

    char2freq, char2_wordset,  char2_doccount, word2freq, char3gram2freq \
        = compute_frequency_counts(path_to_corpus, char_set, end_of_doc_sign, num_process, batch_size)

    # compute relative probability using frequency count collected
    #char_freq_smoothed,  char_word_freq_smoothed, char_doc_freq_smoothed  = compute_prob(char2freq, char2_wordset, char2_doccount, char_set)

    char_freq_smoothed,  char_word_freq_smoothed, char_doc_freq_smoothed  = compute_smoothed_freq(char2freq, char2_wordset, char2_doccount, char_set)


    #pickling frequency counts for other program's usage
    if pickle_flag:
        pickle_statistics(char2freq, char2_wordset, char2_doccount, word2freq, char3gram2freq, path_to_corpus, char_cvs)

    print 'Job finished in ', time.time()-x, ' seconds!'

    return char2freq, char2_wordset, char2_doccount, word2freq, char3gram2freq,\
           char_freq_smoothed,  char_word_freq_smoothed, char_doc_freq_smoothed




def test():
   char_cvs='../working_data/2500.Chinese.Lexicon.Project.csv'
   path_to_corpus='../working_data/sample.seg'
   end_of_doc_sign= 'ZZZENDOFDOCUMENTZZZ'
   #output='../working_data/new.tmp.txt'


   char2freq, char2_wordset, char2_doccount, word2freq, char3gram2freq,char_prob, char_word_prob, char_doc_prob \
       = main(char_cvs, path_to_corpus, end_of_doc_sign, False)

   #print map(len, [char2freq, char2_wordset, char2_doccount, word2freq, char3gram2freq,char_prob, char_doc_prob, char_word_prob])
   return char2freq, char_prob

#test()

if __name__=='__main__':
    print '\n\n>>Excecuting module large-scale ststistics...'
    print '\nArg: 1.target_char.cvs, 2.corpus, 3. end_of_doc_sign, 4(optional) whether the pickle is done '

    print '# of Arg=', len(sys.argv)

    if len(sys.argv)<5:
        pickle_flag = False

    else:
        if str(sys.argv[4]).lower()=='false':
            pickle_flag = False
        else:
            pickle_flag = True


    main(sys.argv[1], sys.argv[2], sys.argv[3], pickle_flag)




