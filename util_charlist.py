#-*- coding: utf-8 -*-
__author__ = 'jma'



import codecs, sys

def  extract_char_from_cvs(char_cvs, path_char_list):

    with codecs.open(char_cvs, 'rU', 'utf-8') as f:


        char_info_list = [line.strip().split(',') for line in f.readlines()[1:]]

        char_set = set([i[0] for i in char_info_list])

    print 'num of character types in the 2500 cvs file:', len(char_set)

    with codecs.open(path_char_list, 'w','utf-8') as f:
        for char in char_set:
            f.write(char+u'\n')




def merge_two_char_files(path1, path2, path3):
    with codecs.open(path1, 'rU','utf-8') as f:
        char_set = set([token for line in f for token in line.split()])


    with codecs.open(path2, 'rU', 'utf-8') as f:
        char_set2 = set([token for line in f for token in line.split()])


    set3 = char_set.union(char_set2)

    with codecs.open(path3, 'w', 'utf-8') as f:
        for char in set3:
            f.write(char+u'\n')

    print 'done! distinct char type count in file1/file2/(merged)file3=', len(char_set), len(char_set2), len(set3)




def set_of_letter_punct_symbol():

    odd = set()

    for i in range(12289, 12353):  #special symbol
        odd.add(unichr(i))

    for i in range(33, 127): #half-width English letters and symbols, puncts
        odd.add(unichr(i))

    for i in range(65281, 65374): # full-width letters and symbols, punctus.
        odd.add(unichr(i))


    y= [u"◆",u"•",u"…",u"∶",u"￥",u"×",u"ß",u"·",u"□",u"‘",u"━", u"–",u"“",u"※",u"Ⅱ",u"─",u"•",u"∶",u"●",u"’",u"”",u"～",u"○",u"◆",u"■"]
    odd.upate(set(y))

    return odd




def select_cyk_chars(char_set):

    new_char_set = set()

    for char in char_set:

        code = ord(char)

        if ord(char)<19967 or ord(char)>40959:
            pass
        else:
            new_char_set.add(char)

    return new_char_set



def pick_good_char_from_file(path_input, path_output):
    with codecs.open(path_input, 'rU', 'utf-8') as f:
        char_set = set([char for line in f for token in line.split() for char in token])

        good_char_set = select_cyk_chars(char_set)

        print 'original charset size=', len(char_set), 'valid CYK charset size =', len(good_char_set)

    with codecs.open(path_output, 'w','utf-8') as f:
        for char in good_char_set:
            f.write(char+u'\n')

    print 'has written the good char list to ', path_output



if __name__ == '__main__':
    print 'extract the list of chars from the 2500 char list of the reaction time data...'

    extract_char_from_cvs(sys.argv[1], sys.argv[2])