# -*- coding: utf-8 -*-
#
# subset_1273.py
#
# subset the result for the 1273 characters in concern
#
import codecs, sys

path_1273 = '../input_data/1273.Chars.For.Freq.Evaluation.Dist.csv'
with codecs.open(path_1273, 'rU', 'utf-8') as f:
    char_set = set([line.split(',')[1] for i, line in enumerate (f) if i>0])
    print 'char count in the small list:',len(char_set)


def subset1273(path, target_char_set):

    

    with codecs.open(path,'rU','utf-8') as f:
        corpus = []
        
        for i, line in enumerate(f):
            token_list = line.split(',')

            if token_list[0] in target_char_set:
                new_token_list = [ u'0.0' if token == u'.' else token for token in token_list  ]

                if token_list == new_token_list:
                    corpus.append(line)
                else:
                    print 'fix line ', i, 'invalid format:', line
                    corpus.append(u','.join(new_token_list)+'\n')

            elif i ==  0:
                print 'title line:',line
                corpus.append(line)


    print 'new corpus size=',len(corpus)

    path_out = path+'.small'
    print 'writing result to', path_out

    with codecs.open(path_out,'w', 'utf-8') as f:
        for line in corpus:
            f.write(line)

    print 'done!'    



if __name__=='__main__':
    print '\nconv select reaction time data for those character in the 1273 char list...'
    print '@Arg: input_path  output will be input_path.small'
    subset1273(sys.argv[1], char_set)

