# -*- coding: utf-8 -*-

'''
Word-based model of ncount and ans from word2vec stats currently have 6 columns about ncount and ans, namely:

sum_ncount, avg_ncount, median_ncount, sum_ans, avg_ans, median_ans

so if we only want 2 values of  ncount, ans

we have 3*3 possibilities

gennerate thsese files automatically.

'''
import codecs, sys

def w2v_word_model_ans_postprocessing(path_result_csv):
    '''

    :param path_result_csv:
    :return:
    '''

    result_map = {i:[] for i in range(9)}

    with codecs.open(path_result_csv,'rU','utf-8') as f :
        lines = f.readlines()
        head_str =','.join(lines[0].strip().split(',')[:-6])+', ncount, ans\n'

        for line in lines[1:]:
            record = line.strip().split(',')
            constant_record = record[:-6]
            ncount_variant = record[-6:-3]
            ans_variant = record[-3:]

            for i, ncount in enumerate(ncount_variant):
                for j, ans in enumerate(ans_variant):
                    #print i*3+j, ncount, ans
                    result_map[i*3+j].append(','.join(constant_record+[str(float(ncount)+1.0), ans])+'\n') #add one smoothing here,
                    #print i*3+j, ','.join(constant_record+[ncount, ans])+'\n'

    suffix=[]
    for w1 in ['sum','avg','med']:
        for w2 in ['sum','avg','med']:
            suffix.append('.'+w1+'_'+w2+'.comb')
    for i in result_map:
        output = path_result_csv+suffix[i]
        with codecs.open(output,'w','utf-8') as f:
            f.write(head_str)
            for record in result_map[i]:
                f.write(record)

    print 'pose processing for word-model done!'
    print 'resulting csv saved to ', path_result_csv, '+ suffix of sum/avg/med and ends with .combi'





if False:
    target_list=[
        'rt.stat.sogou.all.word_model.data0.351884245872',
        'rt.stat.sogou.all.word_model.data0.399335384369',
        'rt.stat.sogou.all.word_model.data0.460443615913',
        'rt.stat.sogou.all.word_model.data0.505957424641',
        'rt.stat.sogou.all.word_model.data0.574473142624',
        ]

if False:
    target_list =['rt.stat.sogou.all.word_model.part.hidex1']

#if True:



print '''\n\n
WARNING: If N/A error happens in the later state of linear modeling, go to line39 and change
result_map[i*3+j].append(','.join(constant_record+[ncount, ans])+'\n')  ===>
result_map[i*3+j].append(','.join(constant_record+[str(int(ncount)+1), ans])+'\n')
(simple add one smoothing, as we will use log(ncount) instead of raw ncount)
'''

if __name__=='__main__':
#for target in target_list:
    w2v_word_model_ans_postprocessing(sys.argv[1])
