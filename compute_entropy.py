__author__ = 'jma'
# coding: utf-8
import cPickle as pickle
import math, codecs, time, os
from large_corpora_stat_gen_for_qling import compute_smoothed_freq
from multiprocessing import Pool

def float2str(sth):
    if type(sth) is float:
        return u"{0:.8f}".format(sth)

    elif type(sth) is unicode:
        return sth.strip()

    else:
        return unicode(sth)

#
# write character and their stats, without outputing original re-action time
#
def write_stat(char_set, char_prob, char_word_prob, char_doc_prob, char2entropy, new_head2ent, new_tail2ent, trigram_ent, trigram_count, output):
    print 'len char2entropy, head2ent, tail2ent, trigram_context=', len(char2entropy), len(new_head2ent), len(new_tail2ent)
    print '\nResult:', u"char, char_prob, char_word_prob, char_doc_prob, char_ent, head_ent, tail_ent, trigram_ent, trigram_context"+u'\n'

    with codecs.open(output, 'w','utf-8') as f:

        f.write( u"char, char_prob, char_word_prob, char_doc_prob, char_ent, head_ent, tail_ent, trigram_ent, trigram_context"+u'\n')
        char_list = list(char_set)
        char_list.sort()

        for i, char in enumerate(char_list):

            cvs_line_str = u",".join(map(float2str,[char_prob[char], char_word_prob[char], char_doc_prob[char],\
                                                    char2entropy[char], new_head2ent[char], new_tail2ent[char], trigram_ent[char],trigram_count[char]]))
            f.write(char+u","+cvs_line_str+ u"\n")
            print  char+u","+cvs_line_str


#from entropy_cal_from_freq import entropy, compute_char_entropy, compute_head_and_tail_entropy
def entropy(list_of_prob):

    if len(list_of_prob)==1:
        return 0.0

    return -sum([prob*math.log(prob,2) for prob in list_of_prob])

def compute_char_entropy(char2word, word2freq):
    char2entropy={}
    for char in char2word:
        freq_list=[word2freq[word] for word in char2word[char]]
        #print '\n\n# char=', char

        #print 'freq list:'
        #for word in char2word[char]:
        #    print word, word2freq[word]

        freq_sum=float(sum(freq_list))
        prob_list=[freq/freq_sum for freq in freq_list]
        #print 'prob list', prob_list
        char2entropy[char]=entropy(prob_list)
        #print 'entropy', char2entropy[char]

    return char2entropy


def compute_head_and_tail_entropy(list_of_word_and_freq_tuples):

    #
    #build tables
    #
    word2freq, head2word, tail2word = {},{},{}
    for word, freq in list_of_word_and_freq_tuples:
        word2freq[word]=freq
        head2word.setdefault(word[0],set()).add(word)
        tail2word.setdefault(word[-1],set()).add(word)

    #
    # compute entropy
    #
    head2entropy=compute_char_entropy(head2word, word2freq)
    tail2entropy=compute_char_entropy(tail2word, word2freq)


    #return head2entropy
    print '\nEntropy computing finished!'

    return head2entropy, tail2entropy


def derive_auxilary_data(char2wordSet, word2freq):
    bichar_word_freq_list = []
    for word in word2freq:
        if len(word) == 2:
            bichar_word_freq_list.append((word, float(word2freq[word])))

    char2_twocharword = {}
    for char in char2wordSet:
        new_set = {word for word in char2wordSet[char] if len(word) == 2}
        char2_twocharword[char] = new_set

    return bichar_word_freq_list, char2_twocharword


def read_pickle_compute_freq(parameter_list):

    print '\nReading pickled tables from ', parameter_list

    head_str=u""

    path_to_word2freq_pickle, path_to_char2wordSet_pickle, path_to_char2freq_pickle, path_to_trigram2freq_pickle,  path_char2doc_pickle, char_cvs = parameter_list

    with codecs.open(char_cvs, 'rU', 'utf-8') as f:

        if '2500' in char_cvs:
            char_info_list = [line.split(',') for line in f.readlines()]
            head_str=char_info_list[0]
            char_info_list = char_info_list[1:]
        elif '1273' in char_cvs:
            char_info_list = [line.split(',')[1:] for line in f.readlines()]
            head_str = char_info_list[0]
            char_info_list = char_info_list

        else:
            print '\Error! The input_data is not the recognized file!'

    char_set = set([i[0] for i in char_info_list])

    f = open(path_to_char2freq_pickle, 'rb')
    char2freq = pickle.load(f)
    f.close()

    f = open(path_to_word2freq_pickle, 'rb')
    word2freq = pickle.load(f)
    f.close()

    f = open(path_to_char2wordSet_pickle, 'rb')
    char2wordSet = pickle.load(f)
    f.close()

    f=open(path_char2doc_pickle, 'rb')
    char2doc = pickle.load(f)
    f.close()


    print 'reading trigram2freq pickle file (can be big)...'
    with open(path_to_trigram2freq_pickle,'rb') as f:
        trigram2freq = pickle.load(f)



    return char_info_list, head_str, char_set, char2freq, word2freq, char2wordSet, trigram2freq, char2doc



def retrieve_pickled_freq(parameter_list):

    print '\nReading pickled tables from ', parameter_list



    path_to_word2freq_pickle, path_to_char2wordSet_pickle, path_to_char2freq_pickle, path_to_trigram2freq_pickle, path_char2doc_pickle = parameter_list




    f = open(path_to_char2freq_pickle, 'rb')
    char2freq = pickle.load(f)
    f.close()

    f = open(path_to_word2freq_pickle, 'rb')
    word2freq = pickle.load(f)
    f.close()

    f = open(path_to_char2wordSet_pickle, 'rb')
    char2wordSet = pickle.load(f)
    f.close()

    f=open(path_char2doc_pickle, 'rb')
    char2doc = pickle.load(f)
    f.close()


    print 'reading trigram2freq pickle file (can be big)...'
    with open(path_to_trigram2freq_pickle,'rb') as f:
        trigram2freq = pickle.load(f)



    print 'num of char types in in char2freq, char2wordSet, char2doc = ', len(char2freq), len(char2wordSet), len(char2doc)

    char_set = set(char2freq.keys()).intersection(set(char2wordSet.keys())).intersection(set(char2doc.keys()))

    print 'num of char types that have records=', len(char_set)



    return char_set, char2freq, word2freq, char2wordSet, trigram2freq, char2doc



#def tmp(char2_twocharword, word2freq, bichar_word_freq_list)


def do_char_head_tail_entropy (bichar_word_freq_list, char2wordSet, char2freq, char_set, word2freq):



    print 'char2freq, char_set len=', len(char2freq), len(char_set)


    print 'Compute char, head and tail entropy..'
    char2entropy = compute_char_entropy(char2wordSet, word2freq)

    # head entropy, tail entropy only with regard to bichar word
    head2entropy, tail2entropy = compute_head_and_tail_entropy(bichar_word_freq_list)


    #
    # as some char does not appear as head or tail, we need fill the missing value with "NA"
    #
    new_head2ent={}
    new_tail2ent={}
    for char in char_set:
        if char in head2entropy:
            new_head2ent[char] = head2entropy[char]
        else:
            new_head2ent[char] = "NA"

        if char in tail2entropy:
            new_tail2ent[char] = tail2entropy[char]

        else:
            new_tail2ent[char] = "NA"



    return  char2entropy,  new_head2ent, new_tail2ent




def do_trigram_entropy(trigram2freq, char_set):

    print 'compute char-trigram entropy...'

    #print 'size of trigram2freq', len(trigram2freq)

    char2trigram = {}
    for trigram in trigram2freq:
        char2trigram.setdefault(trigram[1], set()).add(trigram)

    char_trigram_count = {}
    trigram_ent={}

    for char in char2trigram:

        freq_list=[trigram2freq[trigram] for trigram in char2trigram[char]]
        freq_sum=float(sum(freq_list))

        prob_list=[freq/freq_sum for freq in freq_list]


        #print len(prob_list)

        ent=entropy(prob_list)
        trigram_ent[char] = ent

        if ent<0:
            print '>> error! entropy<0', char, ent, len(char2trigram)
            print prob_list


    new_trigram_ent={}

    for char in char_set:
        if char in trigram_ent:
            new_trigram_ent[char] = trigram_ent[char]
            char_trigram_count[char] = len(char2trigram[char]) + 1
        else:
            new_trigram_ent[char] = "NA"
            char_trigram_count[char] = 1

    print 'done'
    return new_trigram_ent, char_trigram_count



def main(name_list, output):

    x=time.time()

    char_set, char2freq, word2freq, char2wordSet, trigram2freq, char2doc = retrieve_pickled_freq(name_list)

    char_prob, char_word_prob,  char_doc_prob  = compute_smoothed_freq(char2freq, char2wordSet, char2doc, char_set)

    bichar_word_freq_list, char2_twocharword = derive_auxilary_data(char2wordSet, word2freq) #auxiliary data

    char2entropy,   new_head2ent, new_tail2ent = \
        do_char_head_tail_entropy (bichar_word_freq_list, char2_twocharword, char2freq, char_set, word2freq)

    trigram_ent, char_trigram_count= do_trigram_entropy(trigram2freq, char_set)

    print '\n\n>> Computation done! Writing output to', output, '...'
    #write_output(char_info_list, head_str, char_prob, char_word_prob, char_doc_prob, char2entropy, new_head2ent, new_tail2ent, trigram_ent, char_trigram_count,  output)
    write_stat(char_set, char_prob, char_word_prob, char_doc_prob, char2entropy, new_head2ent,new_tail2ent,\
               trigram_ent, char_trigram_count, output)

    print '\n\nJob finished in ', time.time()-x, ' seconds!'

    os.system ('echo "the compute_entropy.py task is done in '+ str(time.time()-x) + ' seconds " | mail -s entropy_taskdone jianqiang.ma@uni-tuebingen.de')



def main2(para):
    name_list, output = para
    main(name_list, output)



if False:

    def test():
        name_list5=[ './working_data/'+i for i in ['sogou.seg.word2freq.output.pickle','sogou.seg.char2wordSet.output.pickle',\
                                                  'sogou.seg.char2freq.output.pickle', 'sogou.seg.char3gram2freq.output.pickle',\
                                                  'sogou.seg.char2docCount.output.pickle', '2500.Chinese.Lexicon.Project.csv']]

        name_list4 =[ './working_data/'+i for i in ['cnWeb.seg.word2freq.output.pickle','cnWeb.seg.char2wordSet.output.pickle',\
                                                  'cnWeb.seg.char2freq.output.pickle', 'cnWeb.seg.char3gram2freq.output.pickle',\
                                                  'cnWeb.seg.char2docCount.output.pickle', '2500.Chinese.Lexicon.Project.csv']]

        name_list1 =[ './working_data/'+i for i in ['subtitle.seg.word2freq.output.pickle','subtitle.seg.char2wordSet.output.pickle',\
                                                  'subtitle.seg.char2freq.output.pickle', 'subtitle.seg.char3gram2freq.output.pickle',\
                                                  'subtitle.seg.char2docCount.output.pickle', '2500.Chinese.Lexicon.Project.csv']]

        name_list3 =[ './working_data/'+i for i in ['cnGigaword.seg.word2freq.output.pickle','cnGigaword.seg.char2wordSet.output.pickle',\
                                                  'cnGigaword.seg.char2freq.output.pickle', 'cnGigaword.seg.char3gram2freq.output.pickle',\
                                                  'cnGigaword.seg.char2docCount.output.pickle', '2500.Chinese.Lexicon.Project.csv']]

        name_list2=[ './working_data/'+i for i in ['baike.clean.seg.word2freq.output.pickle','baike.clean.seg.char2wordSet.output.pickle',\
                                                  'baike.clean.seg.char2freq.output.pickle', 'baike.clean.seg.char3gram2freq.output.pickle',\
                                                  'baike.clean.seg.char2docCount.output.pickle', '2500.Chinese.Lexicon.Project.csv']]

        output5= "./working_data/sogou.fri"
        output4 = "./working_data/web.fri"
        output1= "./working_data/sub.fri"
        output3= "./working_data/giga.fri"
        output2="./working_data/baike.fri"

        tuple_list = [(name_list1, output1), (name_list2,output2), (name_list3,output3), (name_list4,output4), (name_list5,output5)]

        pool = Pool (5)
        pool.map(main2, tuple_list)
        pool.close()
        pool.join()

        #for n, o in tuple_list:
        #    main(n, o)



    def test2():

        name_list=[ '../sogouT.seg/'+i for i in ['sogou.page30to49.seg.2500.Chinese.Lexicon.Project.csv.word2freq.output.pickle','sogou.page30to49.seg.2500.Chinese.Lexicon.Project.csv.char2wordSet.output.pickle',\
                                                  'sogou.page30to49.seg.2500.Chinese.Lexicon.Project.csv.char2freq.output.pickle', 'sogou.page30to49.seg.2500.Chinese.Lexicon.Project.csv.char3gram2freq.output.pickle',\
                                                  'sogou.page30to49.seg.2500.Chinese.Lexicon.Project.csv.char2docCount.output.pickle', '2500.Chinese.Lexicon.Project.csv']]
        output='./sogou.30to49.csv'

        main(name_list, output)



def test_merged_group_a():

    print '\n\n===Compute final Stat (including entropy etc) for the merged pickle group A, containing results from original page 30to49 and 100to155..'

    name_list = ['/home/jma/sogouT.stat/z_merged_pickle/s.merge.groupA.30t49.100t155.'+i for i in \
                 ['word2freq.output.pickle','char2wordSet.output.pickle','char2freq.output.pickle',\
                  'char3gram2freq.output.pickle', 'char2docCount.output.pickle']]

    output = '/home/jma/sogouT.stat/z_merged_pickle/s.merge.groupA.30t49.100t155.stat.csv'

    main(name_list, output)

def test_merged_group_b():

    print '\n\n===Gen final Stat (including entropy etc) for the merged pickle group A, containing results from original page 30to49 and 100to155..'

    name_list = ['/home/jma/sogouT.stat/z_merged_pickle/s.merge.groupB.1t29.50t99.156t288.'+i for i in \
                 ['word2freq.output.pickle','char2wordSet.output.pickle','char2freq.output.pickle',\
                  'char3gram2freq.output.pickle', 'char2docCount.output.pickle']]

    output = '/home/jma/sogouT.stat/z_merged_pickle/s.merge.groupB.1t29.50t99.156t288.stat.csv'

    main(name_list, output)

def test_merged_sogou_all():
    # have the para for the pickles that contain the freq from the whole sogout corpus

    print '\n\n===Gen final Stat (including entropy etc) for the merged pickle group A, containing results from original page 30to49 and 100to155..'

    name_list = ['/home/jma/sogouT.stat/z_merged_pickle/sogou.all.stat.'+i for i in \
                 ['word2freq.output.pickle','char2wordSet.output.pickle','char2freq.output.pickle',\
                  'char3gram2freq.output.pickle', 'char2docCount.output.pickle']]

    output = '/home/jma/sogouT.stat/z_merged_pickle/sogou.all.stat.csv'

    main(name_list, output)

#test2()

#test_merged_group_a()
#test_merged_group_b()
test_merged_sogou_all()



