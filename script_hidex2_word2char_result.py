# -*- coding: utf-8 -*-
from ncount_ans_predictor_w2v import append_predictor_to_csv
from script_merge_hidex_result import read_hidex_result
import cPickle as pickle, time, os

def word_stat_2_char_stat2(word2nc_ans, char2word_map):
    type_of_predictor =2
    #step = type_of_predictor*len(list_of_threshould)

    char2record={}

    for char in char2word_map:
        char2record[char] = []
        words = char2word_map[char]
        list_of_result = [word2nc_ans[word] for word in words if word in word2nc_ans]
        if list_of_result:
            pass
            #print 'len_list_result',len(list_of_result)
            #print  'list_of_result=', list_of_result
            #ncount_list, ans_list = zip(*list_of_result)

        else:
           list_of_result = [[0.0 if i%2 else 1 for i in range(type_of_predictor)]]  # do add-one smoothing for zero count character


        #print list_of_result, len(list_of_result), type_of_predictor  #, len(list_of_threshould)
        predictors={i:[result[i] for result in list_of_result] for i in range(type_of_predictor)}
        for predictor in predictors.values():
            p_sum = sum(predictor)
            p_avg = p_sum/float(len(predictor))
            p_median = predictor[len(predictor)/2]


            char2record[char].extend([p_sum, p_avg, p_median])


    return char2record


def tranform_word_result2char_result(path_hidex_word_result, char2word_pkl, path_csv, path_new_output):

    time_start = time.time()

    word2nc_ans = read_hidex_result(path_hidex_word_result)

    print 'word2nc_ans', word2nc_ans.keys()[0], word2nc_ans[word2nc_ans.keys()[0]], len(word2nc_ans)


    #list_of_threshold=[0.05*i for i in range(21)]
    #print 'list of threshold=', list_of_threshold, len(list_of_threshold)

    char2word_map = pickle.load(open(char2word_pkl,'rb'))

    char2nc_ans = word_stat_2_char_stat2(word2nc_ans, char2word_map)

    print '\n'*3, '====  Results ====='
    for char in char2nc_ans:
        print char, char2nc_ans[char]


    print '\n'*3, '>> Appending the new predictors to the old csv', path_csv, 'and writing to new csvs with prefix:', path_new_output
    append_predictor_to_csv(char2nc_ans, path_csv, path_new_output, [1])

    time_diff = time.time() - time_start
    #os.system ('echo "the ncount+ ans: word model task is done in '+ str(time_diff) + ' seconds " | mail -s word-model@suebi jianqiang.ma@uni-tuebingen.de')



#
# sth to-do here...
#
if False:
    path_new_output = '../input_data/rt.stat.sogou.all.word_model.part.hidex'
    path_hidex_word_result = '../input_data/ans.hidex.word.part.txt'
    char2word_pkl = '../working_data/hidex.char2word.pkl'
    path_csv = '../input_data/rt.stat.sogouT.csv'
if True:
    path_new_output = '../input_data/rt.stat.sogou.all.word_model.100k.hidex'
    path_hidex_word_result = '../input_data/ans.hidex.word.100k.txt'
    char2word_pkl = '../working_data/hidex.char2word.size100000.pkl'
    path_csv = '../input_data/rt.stat.sogouT.csv'
tranform_word_result2char_result(path_hidex_word_result, char2word_pkl, path_csv, path_new_output)