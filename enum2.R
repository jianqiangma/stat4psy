#
## try an iteration of the linear regression by trying all combinations of the variables as predictors
#

#library (DAAG)
#library(plyr)
library(formula.tools)

enum_all_models <- function(d){
  #print (length(d))
  y = d[,1]
  for (i in 2:length(d)-1){
    #print ('****')
    #print (i)
    for (j in (i+1):length(d)){
      #print ('---')
      #print (j)
      x= data.frame(y, d[,i:j])
      fit = lm(x)
      #print (length(d[,i:j]))
      print (summary(fit))
      
    }
  }
}



# Pre-processing;
#  1. manually (or previously) change line 629 (starting from 1) (character 顸 ) to convert . to 0.0 to make is recognizable as float number  
#  2. line 2044 (character 谖)  has 0 for acc, ntrials, etc, ==> discard this line
#  3. convert N/A entropies into -10 v.s. 0 (see the difference)
#  4. convert char_prob, char_word_prob and char_doc_prob into log(prob), as the raw prob distr. is Zipfian rather than Gaussian (which LM asssumes) 
#


preprocessing <- function(refined_csv_file){
  d = read.csv(refined_csv_file, header = TRUE, sep=",")
  
  d = subset(d, d$Acc>0)
  print (length(d))
  # deal with very small corpus where 0.0 prob  occurs, which will after log will be -Inf!
  #d[which(d[[8]]==0 | d[[9]]==0 | d[[10]]==0),8:10] = 1   
  
  e = data.frame(d$Z.RT., log(d$char_prob), log(d$char_word_prob), log(d$char_doc_prob), d$char_ent, d$head_ent, d$tail_ent, d$trigram_ent, log(d[[15]]), log(d$ncount), d$ans)
  
  names(e) = c("zrt","char_prob","word_prob","doc_prob","char_ent","head_ent","tail_ent","trigram_ent", "trigram_countext","ncount","ans")
  
  f <- e
  
  #e[is.na(e)] <- 0.0
  f[is.na(f)] <- -1.0
  
  return (f)
  #return (list(f))
  #return (list(e,f))  
} 



#
# credit: enumerate_formula_for_all_combinaitons and gen_models functions are adapt from  "Linear models in R with different combinations of variables": 
# http://stackoverflow.com/questions/22955617/linear-models-in-r-with-different-combinations-of-variables
#

enumerate_formula_for_all_combinations <- function (reaction_time_data) {
  variable_size = 11
  vars <- names(reaction_time_data)
  N <- list(1,2,3,4,5,6,7,8,9,10)
  COMB <- sapply(N, function(m) combn(x=vars[2:variable_size], m))
  
  formula_list <- list()
  k=0
  for(i in seq(COMB)){
    tmp <- COMB[[i]]
    for(j in seq(ncol(tmp))){
      k <- k + 1
      formula_list[[k]] <- formula(paste("zrt", "~", paste(tmp[,j], collapse=" + ")))
    }
  }
  
  formula_list
  
}



gen_models <- function(data, formula_list){
  model_list <- vector(mode="list", length(formula_list))
  for(i in seq(formula_list)){
    #print (formula_list[[i]])
    model_list[[i]] <- lm(formula_list[[i]], data=data)
  }
  model_list
}




lm_by_enum <- function(refined_csv_file) {
  
  
  reaction_time_data = preprocessing (refined_csv_file)
  r_m_list = lm_by_enum_without_preproc(reaction_time_data)
  return (r_m_list)

}




lm_by_enum_without_preproc <- function(d) {

  formula_list = enumerate_formula_for_all_combinations (d)
  #print (formula_list)  
  
  model_list = gen_models(d, formula_list)  
  #for (model in model_list){
  #  print (summary(model))
  #}
  
  m=model_list
  
  sum1= 0
  score1 = numeric()
  formula_str = character()
  
  
  for (i in 1:length(m)){
    r_score= summary(m[[i]])$adj.r.squared
    #print (r_score)
    sum1 = sum1 + r_score
    score1 = c(score1, r_score)
    
    formula_str = c(formula_str, as.character(formula_list[i]))

    #print (formula_list[[i]])
  }
  
  
  result1=data.frame(score1, formula_str)
  
  names(result1) = c("adj.r.squared","formula.used")
  
  print (names(result1))
  
  r1 = result1[order(-result1$adj.r.squared),]
  
  cat("\n\n\n")
  print ('======= top candidate of resulting models ====')
  for (i in 1:3){
    print ('------')
    print (i)
    r= r1[i, ]
    id = as.numeric(rownames(r))
    print (r)
    print (id)
    print (summary(m[[id]]))
    
  }
  
  #print (length(r1))
  #print (sum1)
  #print (r1[1,])
  
  #print (sum2)
  #print (r2[1,])
  
  #cv_result = cv.lm(df=reaction_time_data_list[[1]], m[[1]][[101]], m=5)
  
  
  
  #print(summary(r1))
  #print (length(r1))
  
  return (list(r1, m))
  
}


