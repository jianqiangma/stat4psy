# -*- coding: utf-8 -*-

import codecs, os

def run_r_script(directory, suffix):

    csv_list = [ os.path.abspath(directory)+'/'+file for file in os.listdir(directory) if file[-len(suffix):] == suffix]
    if csv_list:
        for csv_file in csv_list:
            os.system("Rscript  script.lm.PCA.R "+csv_file)




run_r_script('../input_data','.comb')