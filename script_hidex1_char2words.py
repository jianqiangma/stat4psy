# -*- coding: utf-8 -*-
#from script_merge_hidex_result import read_hidex_result, append_predictor_to_csv

from ncount_ans_predictor_w2v import read_reaction_time_csv_2500
import cPickle as pickle
'''
construct a mapping from character to words

'''

import codecs
def read_hidex_dict(path):
    word_freq=[]
    with codecs.open(path,'rU','utf-8') as f:
        for line in f:
            word, id, freq_str =  line.strip().split()
            freq = int(freq_str)
            word_freq.append((word, freq))
    return word_freq





def char2words(reaction_time_csv, hidex_dict_path, max_type_size):
    char_list = read_reaction_time_csv_2500(reaction_time_csv)
    word_freq = read_hidex_dict(hidex_dict_path)

    word_freq.sort(key=lambda x:x[1], reverse=True)
    word2freq = {}
    for word, freq in word_freq[:min(max_type_size, len(word_freq))]:
        word2freq[word]  = freq




    char2word_map={char:set() for char in char_list}
    words_of_interests = set()

    #word_list_ans = word_list[:min(max_type_count, len(word_list))]
    for word in word2freq:
        for char in word:
            if char in char2word_map:
                char2word_map[char].add(word)
                words_of_interests.add(word)

    list_of_words= list(words_of_interests)
    print '\nNum of words of interests=', len(list_of_words)

    path_out = '../working_data/words.target.size'+str(max_type_size)+'.txt'
    print 'writing word list to ', path_out
    with codecs.open(path_out, 'w','utf-8') as f:
        for word in words_of_interests:
            f.write(word+'\n')


    hidex_charword_path='../working_data/hidex.char2word.size'+str(max_type_size)+'.pkl'
    print 'pickling char2word map to', hidex_charword_path
    pickle.dump(char2word_map, open(hidex_charword_path,'wb'))

    print 'done'

r_csv, h_dict, max_type ='../working_data/2500.Chinese.Lexicon.Project.csv',  '../working_data/sogouT.full.dict', 100000
char2words(r_csv,h_dict, max_type)

