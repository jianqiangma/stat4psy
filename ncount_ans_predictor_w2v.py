# -*- coding: utf-8 -*-
'''
Generate Ncount and ANS as predictors for reaction time of Chinese characters using word2vec similarities

Preliminary:
Ncount and ANS are originally proposed in Cyrus Shaoul's Hidex (see https://github.com/cyrus/high-dimensional-explorer/blob/master/HowToUseHiDEx.txt)
NCOUNT: The number of neighbors, defined by the threshold.
ANS: Average Neighbour Similarity (The average similarity between the word and its neighbors as defined above

We implement 3 approaches:


1. character-based approach (use-character based word2vec model):

(1) generate threshold T via a predecure called "sim-rank":

- compute 1 million character-wise similarity using top 1000 most frequent characters
- rank the sim socre, and define the threshold T as the lowest sim score of the top1% highest sim scores.

(2) Neighbor selection and Ncount-ANS computation

called "gen_ncount_ans":

For each character c, find all the characters c' such that sim(c, c') >= T, those character c' are defined as Neighbors
For each character c, we keep a list, neighbor_list,  of tuples, (c', sim(c,c')).

Ncount = len(neighbor_list)
ANS = sum(all sim(c, c') in neighbor_list)/Ncount (Ncount>0),  0 (Ncount = 0)?



2. word-based approach (use word-based word2vec model):

(1) generate threshold T in a similar manner, as in character-based case, but instead, use the top 1000 most frequent WORD instead

(2) for each character c, for each word that contains this character, compute NCount and ANS, we have a list of scores  then avg/median/max to get one score?


3. character-as-word approach (use word-based word2vec model):

(1) generate threashold T from top 1000 most frequent words, but for (2) compute Ncount and ANS only for single-character words.

Note this may leads to several characters that have zero Ncount and thus 0 or undefined ANS

'''
import codecs, multiprocessing, numpy
import cPickle as pickle
from bisect import bisect_left
from gensim.models.word2vec import *
from copy import deepcopy



#  ======================  BELOW:  auxiliary functions, binary_search(), read_reaction_time_csv_2500(),
# append_predictor_to_csv(), and prune_w2v ==========

def binary_search(a, x, lo=0, hi=None):
    hi = hi or len(a)
    pos = bisect_left(a,x,lo,hi)
    #return (pos if pos != hi else -1)
    return pos



def read_reaction_time_csv_2500(reaction_time_csv):
    '''
    read the csv file tha contains the raction time data of the 2500 characters
    '''
    print '\nread the original reaction time data to get character list...'
    with codecs.open(reaction_time_csv, 'rU', 'utf-8') as f:
        lines= f.readlines()
        if '2500' in reaction_time_csv:
            char_list =[ line.split(',')[0] for line in lines[1:]]
        else:
            print '\Error! The input_data is not the recognized file!', reaction_time_csv


    return char_list



def append_predictor_to_csv(char2nc_ans, path_csv, path_new_output, threshold_list):
    '''
    :param char2nc_ans: map, key=char, value=a list of NCount and ANS for the char
    :param path_csv: the CSV format of the time reaction and corpus data
    :param path_new_output: the csv format of the data appended by the two new predictors
    :return:
    '''

    f=codecs.open(path_csv,'rU','utf-8')
    lines = f.readlines()
    head_str = lines[0]
    records = lines[1:]
    f.close()



    pred_num = len(char2nc_ans.values()[0])/len(threshold_list)
    print 'pred_num=', pred_num
    for i, threshold in enumerate(threshold_list):
        new_head_str = head_str.strip()+','+','.join(['p'+str(num) for num in range(pred_num)])+'\n'

        path_new_output2=path_new_output+str(threshold)
        with codecs.open(path_new_output2, 'w','utf-8') as f:
            f.write(new_head_str)
            for line in records:
                char=line.split(',')[0]
                if char in char2nc_ans:
                    line.strip()
                    new_line = line.strip()+','+ ','.join(map(str, char2nc_ans[char][i*pred_num:(i+1)*pred_num]))+'\n'

                else:
                    new_line = line.strip()+','+','.join(['0.0' for num in range(pred_num)])+'\n'
                    print 'Warning: character', char, 'is not in the record!'

                f.write(new_line)

        print '\nAppending the Ncount and ANS predictors group', i+1 ,'to csv, result@', path_new_output2



def prune_w2v(model, max_word_num):
    '''

    :param model: a word2vec model
    :param max_word_num: maximum number of words, if the size of model.vocab is larger than this number, only the top max_word_num words will be
    :return: None. but the model is pruned now, only keeping top max_word_num most freq words and their vectors as in model.syn0
    '''

    if model.hs:
        model.syn1 = None
    else:
        model.syn1neg = None
        model.table = None


    # need to update model.vocab and model.index2word

    word_list = model.vocab.keys()
    if len(word_list)>max_word_num:
        new_word_list = sorted(word_list, key=lambda x:model.vocab[x].count, reverse=True)[:max_word_num]


    vocab={}
    index2word=[]
    syn0= numpy.empty((max_word_num, model.layer1_size), dtype=REAL)

    for word in new_word_list:
        v = model.vocab[word]
        vector = model.syn0[v.index]

        index = len(vocab)

        syn0[index] = vector
        index2word.append(word)
        vocab[word] = Vocab(count = v.count, index = index)

    model.syn0 = syn0
    model.vocab = vocab
    model.index2word = index2word


#  ======================  BELOW:  core functions of the code, sim_rank() and gen_ncount_ans() ==========

def sim_rank(w2v):
    '''
    this is a naive implementaiton, very slow for large lexicon (> millions of words). In that case, it is recommended to
    (1) prune the word2vec model (2) use compute_top_sim_single_process() for this purpose

    :param w2v: Python gensim implementation of word2vec model, already trained
    :return: threshold T, which cut at the top1% of sampled 1 million similarity scores
    '''
    k=1000
    inverse_top_percentage = 100


    max_type_count = 200000  # this can be very time consuming, so we only take top 10k for sim computation...
    top_word_list = []


    x=w2v.vocab.items()
    x.sort(key=lambda x:x[1].count, reverse=True)
    top_word_list=[item[0] for item in x[:k]]


    if len(w2v.vocab)>max_type_count:
        z=[item[0] for item in x[:max_type_count] ]

    else:
        z=[w for w in w2v.vocab]

    sim_list = []
    for word in top_word_list:
        for w2 in z:
            if word !=w2:
                sim_list.append(w2v.similarity(word, w2))

    #return sim_list

    return sim_list[max(0,len(sim_list)/inverse_top_percentage-1)]




def gen_ncount_ans(w2v, entry, threshold):
    '''
    Neighbor selection and Ncount-ANS computation
    Find the neighbors of an entry, which are defined as other entries whose sim score with the target entry are above the threshold

    '''

    if False:  # Word2Vec after a certain version all adopt unicdoe as internal representation of strings, so skip this convertions...
        w2v_type= type(w2v.vocab.keys()[0])

        if not type(entry) is w2v_type:
            if type(entry) is unicode:
                entry=entry.encode('utf-8')  #looks that @suebi, everything is kept as byte strings....
            elif type(entry) is str:
                entry = entry.decode('utf-8')
            else:
                print 'unknown type of the entry', entry, 'type is', type(entry)

    top_n = len(w2v.vocab)
    if entry in w2v.vocab:
        neighbor_sim_list = w2v.most_similar(entry, topn=top_n)
        entry_score_pair_list = [ -1.0*score for e, score in  neighbor_sim_list]
        pos = binary_search(entry_score_pair_list, -1.0*threshold)
        neighbor_sim_list = neighbor_sim_list[:pos]

    else:
        print 'entry ',entry, ' is NOT in the w2v vocab!'
        neighbor_sim_list = []

    return neighbor_sim_list





#####################################
#                                   #
# BELOW:  MODEL3: WORD-BASED MODEL  #
#                                   #
#####################################




def word_stat_2_char_stat(word2nc_ans, char2word_map, list_of_threshould):
    type_of_predictor =2
    step = type_of_predictor*len(list_of_threshould)

    char2record={}

    for char in char2word_map:
        char2record[char] = []
        words = char2word_map[char]
        list_of_result = [word2nc_ans[word] for word in words]
        if list_of_result:
            pass
            #print 'len_list_result',len(list_of_result)
            #print  'list_of_result=', list_of_result
            #ncount_list, ans_list = zip(*list_of_result)

        else:
           list_of_result = [[0.0 if i%2 else 1 for i in range(step)]]  # do add-one smoothing for zero count character


        predictors={i:[result[i] for result in list_of_result] for i in range(step)}
        for predictor in predictors.values():
            p_sum = sum(predictor)
            p_avg = p_sum/float(len(predictor))
            p_median = predictor[len(predictor)/2]


            char2record[char].extend([p_sum, p_avg, p_median])


    return char2record



def word_model_multi_process(reaction_time_csv, w2v_path, path_csv, path_new_output, max_type_count ):

    '''
    WORD Model: the multi-process version (it's time consuming, so multi-processing is necessary...)
    '''
    sample_size_for_threshold=1000

    time_start = time.time()

    print 'read char list'
    char_list = read_reaction_time_csv_2500(reaction_time_csv)
    process_num = min(multiprocessing.cpu_count(), 20)


    #threshold = 0.14638  # use fix value for quick over-turn
    print 'Loading word2vec model...'
    w2v = Word2Vec.load(w2v_path)


    if len(w2v.vocab)>max_type_count:
        print 'pruning word2ve model to vocab size of', max_type_count, '.'
        prune_w2v(w2v, max_type_count)

    w2v_path='./tmp.word_model.w2v'
    w2v.save(w2v_path)



    print 'Compute threshold using multiprocessing...'


    word_list = w2v.vocab.items()
    word_list.sort(key=lambda x:x[1].count, reverse=True)
    if len(word_list)>sample_size_for_threshold:
        top_word_list = [ word for  word, entry in word_list[:sample_size_for_threshold]]
    else:
        top_word_list = [word for word, entry in word_list]

    b_size = len(top_word_list)/process_num
    list_of_top_word_list = [top_word_list[i*b_size:(i+1)*b_size] for i in range(process_num)]
    if len(top_word_list)%process_num:
        list_of_top_word_list.append(top_word_list[process_num*b_size:])
    arg_list1=[(w2v_path,top_list, max_type_count) for top_list in list_of_top_word_list ]


    assign_threshold = True

    if not assign_threshold:

        sim_list =[]
        print 'start...'
        pool=multiprocessing.Pool(process_num)
        score_lists = pool.map(compute_top_sim_single_process, arg_list1 )
        pool.close()
        pool.join()

        for l in score_lists:
            sim_list.extend(l)

        sim_list.sort(reverse=True)
        list_of_threshold = [sim_list[max(0,len(sim_list)/10000-1)], sim_list[max(0,len(sim_list)/1000-1)],\
                             sim_list[max(0,len(sim_list)/500-1)],sim_list[max(0,len(sim_list)/200-1)] ,sim_list[max(0,len(sim_list)/100-1)]]

    else:
        print 'Try out pre-defined threshold ranging from 0.1 to 0.95...'
        list_of_threshold= [0.05*i+0.1 for i in range(18)]

   #
   # get a list of "interest-words", which contains at least one character in the character list
   #
    char2word_map={char:set() for char in char_list}
    words_of_interests = set()

    #word_list_ans = word_list[:min(max_type_count, len(word_list))]
    for word in w2v.vocab:
        for char in word:
            if char in char2word_map:
                char2word_map[char].add(word)
                words_of_interests.add(word)

    list_of_words= list(words_of_interests)
    print '\nNum of words of interests=', len(list_of_words)



    #
    # compute Ncount + ANS for these WORDS of interest
    #

    #for threshold in list_of_threshold:
        #threshold = sim_list[max(0,len(sim_list)/100-1)]

    batch_size = len(list_of_words)/process_num
    list_of_word_list =[ list_of_words[i*batch_size:(i+1)*batch_size]  for i in range(process_num)]
    if len(list_of_words)%process_num:
        list_of_word_list.append(list_of_words[process_num*batch_size:])
    arg_list = [(list_of_threshold,c_list, w2v_path) for c_list in list_of_word_list]

    print '\n'*3, 'Multiprocessing for computing predictors...'

    pool = multiprocessing.Pool(process_num)
    results = pool.map(compute_predictor_single_process, arg_list)
    pool.close()
    pool.join()

    word2nc_ans = {}
    for word2record in results:
        for word in word2record:
            word2nc_ans[word] = word2record[word]




    #
    #  convert word ncount, ans to  character ncount, ans...
    #
    # char2nc_ans: key:char value: sum_ncount, avg_ncount, median_ncount, sum_ans, avg_ans, median_ans
    char2nc_ans = word_stat_2_char_stat(word2nc_ans, char2word_map, list_of_threshold)

    print '\n'*3, '====  Results ====='
    for char in char2nc_ans:
        print char, char2nc_ans[char]

    word_model_char_record_pickle = './word.model.record.pickle'
    print '\n'*3, '>> Pickling the full list of neigbhors and the sims to ', word_model_char_record_pickle
    with open(word_model_char_record_pickle, 'wb') as f:
       pickle.dump(char2nc_ans,f)  #pickle the (ncount, nas) instead of the full neighbor list



    print '\n'*3, '>> Appending the new predictors to the old csv', path_csv, 'and writing to new csvs with prefix:', path_new_output
    append_predictor_to_csv(char2nc_ans, path_csv, path_new_output, list_of_threshold)

    time_diff = time.time() - time_start
    os.system ('echo "the ncount+ ans: word model task is done in '+ str(time_diff) + ' seconds " | mail -s word-model@suebi jianqiang.ma@uni-tuebingen.de')









####################################
#                                  #
#  BELOW: MODEL1: CHAR-BASED MODEL #
#                                  #
####################################


def char_model_compute_predictors(char_list, w2v):
    print 'Compute threshold...'
    #threshold = 0.14638  # use fix value for quick over-turn
    threshold = sim_rank(w2v)
    print 'The threshold found is:', threshold

    char2nr_sim_list={}  #map of character to a list that keeps its neighbors and their similarities with the char
    char2nc_ans = {}  #map of character to Ncount and ANS

    print '\nGenerating neigbhors and similarity score for each character'
    for char in char_list:
        print 'current char:', char
        neighbor_sim_list =gen_ncount_ans(w2v, char, threshold)
        #char2nr_sim_list[char] = neighbor_sim_list  #<------ temporarily blocking this complete output for dev...
        char2nc_ans[char]= (len(neighbor_sim_list)+1, sum([s for e, s in neighbor_sim_list])/float(len(neighbor_sim_list)) if neighbor_sim_list else 0.0 )

    return char2nc_ans, char2nr_sim_list, threshold



def character_based_model(reaction_time_csv, w2v_path, path_csv, path_new_output):
    '''
    The Character-based Model
    '''

    print '\nLoading w2v model....'
    w2v=Word2Vec.load(w2v_path)
    char_list = read_reaction_time_csv_2500(reaction_time_csv)

    print '\n===Calling predictor computation===\n'
    char2nc_ans, char2nr_sim_list, threshold = char_model_compute_predictors(char_list, w2v)

    print '\n'*3, '====  Results ====='
    for char in char2nc_ans:
        print char, char2nc_ans[char]


    print '\n'*3, '>> Appending the new predictors to the old csv', path_csv, 'and writing to new csv:', path_new_output
    append_predictor_to_csv(char2nc_ans, path_csv, path_new_output)

    print 'done! (current threashold =',threshold, ')'





########################################
#                                      #
#  BELOW: MODEL2: CHAR AS WORD MODEL   #
#                                      #
########################################


def compute_predictor_single_process(arg_tuple):
    '''
    Character-as-Word Model:  the single-process version of

    '''

    list_of_threshold, item_list, w2v_path = arg_tuple
    print '\nLoading w2v model and read char list....'
    w2v=Word2Vec.load(w2v_path)

    #
    # prune the vocabulary
    #max_type_count = 200000
    #if len(w2v.vocab)>max_type_count:
    #    print 'pruning word2ve model to vocab size of', max_type_count, '.'
    #    prune_w2v(w2v, max_type_count)

    print '\nCompute predictors...'
    item2nc_ans = {}  #map of character to Ncount and ANS

    print '\nGenerating neigbhors and similarity score for each character'
    for item in item_list:
        print 'compute ncount& ans for current item:', item

        record =[]
        for threshold in list_of_threshold:
            neighbor_sim_list = gen_ncount_ans(w2v, item, threshold)
            record.append(len(neighbor_sim_list)+1)
            record.append(sum([s for e, s in neighbor_sim_list])/float(len(neighbor_sim_list)) if neighbor_sim_list else 0.0)


        #char2nr_sim_list[char] = neighbor_sim_list  #<------ temporarily blocking this complete output for dev...

        item2nc_ans[item]= record

    return item2nc_ans



def compute_top_sim_single_process(argstr):

    #print 'compute_top_sim_single: num of arg=',len(argstr), 'arg str summary=:',[i if type(i) is int else len(i) for i in argstr], ' of type:', type(argstr)

    w2v_path, top_word_list,max_type_count = argstr
    w2v = Word2Vec.load(w2v_path)


    #w_v_list=w2v.vocab.items()
    #w_v_list.sort(key=lambda x:x[1].count, reverse=True)
    #if len(w2v.vocab)>max_type_count:
    #    z=[item[0] for item in w_v_list[:max_type_count] ]
    #else:
    #    z=[w for w in w2v.vocab]

    topn = min(len(w2v.vocab), max_type_count)

    sim_list = []
    for word in top_word_list:
        print 'compute top_sim for ', word
        word_score_list = w2v.most_similar(word, topn=topn-1)
        sim_list.extend([score for word, score in word_score_list])

        #for w2 in z:
        #    if word !=w2:
        #        sim_list.append(w2v.similarity(word, w2))

    #print 'sim_list=', sim_list[:10]
    return sim_list




def character_as_word_model_multi_process(reaction_time_csv, w2v_path, path_csv, path_new_output, max_type_count):

    '''
    Character-as-Word Model: the multi-process version (it's time consuming, so multi-processing is necessary...)
    '''

    start_time = time.time()

    sample_size_for_threashold=1000

    print 'read char list'
    char_list = read_reaction_time_csv_2500(reaction_time_csv)
    process_num = min(multiprocessing.cpu_count(), 20)


    #threshold = 0.14638  # use fix value for quick over-turn
    print 'Loading word2vec model...'
    w2v = Word2Vec.load(w2v_path)


    if len(w2v.vocab)>max_type_count:
        print 'pruning word2vec model to vocab size of', max_type_count, '.'
        prune_w2v(w2v, max_type_count)

    w2v_path='./tmp.w2v'
    w2v.save(w2v_path)


    print 'Compute threshold using multiprocessing...'
    #threshold = sim_rank(w2v)


    top_word_list = w2v.vocab.items()
    top_word_list.sort(key=lambda x:x[1].count, reverse=True)
    if len(top_word_list)>sample_size_for_threashold:
        top_word_list = [ word for  word, entry in top_word_list[:sample_size_for_threashold]]

    #print '\n'*5


    b_size = len(top_word_list)/process_num
    list_of_top_word_list = [top_word_list[i*b_size:(i+1)*b_size] for i in range(process_num)]
    if len(top_word_list)>process_num*b_size:
        list_of_top_word_list.append(top_word_list[process_num*b_size:])


    #print '\n'*5, '>>> show list of top_word_list'
    #for i in list_of_top_word_list:
    #    print ' '.join(i)


    arg_list1=[(w2v_path, top_list, max_type_count) for top_list in list_of_top_word_list ]

    #print '\n'*5, '===============   arglist!!!'
    #for a,b,c in arg_list1:
    #    print ' '.join(b)

    sim_list =[]
    #print 'len of arg_list1', len(arg_list1)
    print 'start...'
    pool=multiprocessing.Pool(process_num)
    score_lists = pool.map(compute_top_sim_single_process, arg_list1 )
    pool.close()
    pool.join()

    for l in score_lists:
        sim_list.extend(l)

    sim_list.sort(reverse=True)
    #list_of_threshold = [sim_list[max(0,len(sim_list)/1000-1)],sim_list[max(0,len(sim_list)/200-1)], sim_list[max(0,len(sim_list)/100-1)]]
    list_of_threshold = [sim_list[max(0,len(sim_list)/10000-1)], sim_list[max(0,len(sim_list)/1000-1)],sim_list[max(0,len(sim_list)/500-1)],sim_list[max(0,len(sim_list)/200-1)] ,sim_list[max(0,len(sim_list)/100-1)]]

    #for threshold in list_of_threshold:

    print '\n'*3, 'Predicting use threshold =', list_of_threshold, '...'

    batch_size = len(char_list)/process_num
    list_of_char_list =[ char_list[i*batch_size:(i+1)*batch_size]  for i in range(process_num)]
    if len(top_word_list)>process_num*batch_size:
        list_of_char_list.append(char_list[process_num*batch_size:])
    arg_list = [(list_of_threshold,c_list, w2v_path) for c_list in list_of_char_list]

    print '\n'*3, 'Multiprocessing for computing predictors...'

    pool = multiprocessing.Pool(process_num)
    results = pool.map(compute_predictor_single_process, arg_list)
    pool.close()
    pool.join()

    char2nc_ans = {}
    for char2record in results:
        for char in char2record:
            char2nc_ans[char] = char2record[char]


    print '\n'*3, '====  Results ====='
    for char in char2nc_ans:
        print char, char2nc_ans[char]


    #print '\n'*3, '>> Pickling the full list of neigbhors and the sims to ', char_nr_sim_list_pickle
    #with open(char_nr_sim_list_pickle, 'wb') as f:
    #    pickle.dump(char2nc_ans,f)  #pickle the (ncount, nas) instead of the full neighbor list




    print '\n'*3, '>> Appending the new predictors to the old csv', path_csv, 'and writing to new csv with prefix', path_new_output
    append_predictor_to_csv(char2nc_ans, path_csv, path_new_output, list_of_threshold)

    time_diff = time.time() - start_time
    print 'End in ', time_diff, 'seconds..'

    os.system ('echo "the char-as-word model task is done in '+ str(time_diff) + ' seconds " | mail -s char-as-word@suebi jianqiang.ma@uni-tuebingen.de')




#
#  ================ BELOW: Script for Running three models on Suebi ============
#

def test_word_model():
    reaction_time_csv = '/home/jma/reaction_time/working_data/2500.Chinese.Lexicon.Project.csv'
    w2v_path = '/home/jma/w2v/sogouT.all.seg.dim300.win5.min20.cbow.ns.w2v'
    #w2v_path="tmp.word_model.w2v"
    path_csv = '/home/jma/reaction_time/working_data/rt.stat.sogou.all.csv'
    path_new_output = '/home/jma/reaction_time/working_data/rt.stat.sogou.all.word_model.data'
    max_type_count = 100000
    word_model_multi_process(reaction_time_csv, w2v_path, path_csv, path_new_output, max_type_count)



def test_character_model():

    print '\nRunning character-based model for nc& ans...'
    print '\nWarning: the path are hard-coded, should runnable at jma user directory @suebi'
    reaction_time_csv = '/home/jma/reaction_time/working_data/2500.Chinese.Lexicon.Project.csv'
    w2v_path = '/home/jma/w2v/sogou.100to199.char.dim200.win6.min10.w2v'

    path_csv = '/home/jma/reaction_time/working_data/rt.stat.sogou.all.csv'
    path_new_output = '/home/jma/reaction_time/working_data/rt.stat.sogou.all.new'

    character_based_model(reaction_time_csv, w2v_path, path_csv, path_new_output)



def test_character_as_word_model():

    reaction_time_csv = '/home/jma/reaction_time/working_data/2500.Chinese.Lexicon.Project.csv'
    w2v_path = '/home/jma/w2v/sogouT.all.seg.dim300.win5.min20.cbow.ns.w2v'
    #w2v_path ='tmp.w2v'
    path_csv = '/home/jma/reaction_time/working_data/rt.stat.sogou.all.csv'
    path_new_output = '/home/jma/reaction_time/working_data/rt.stat.sogou.all.char.as.word.multi'

    max_type_count = 200000
    character_as_word_model_multi_process(reaction_time_csv, w2v_path, path_csv, path_new_output, max_type_count )




def word_model_multi_process_manual_threshold(reaction_time_csv, w2v_path, path_csv, path_new_output, max_type_count ):

    '''
    WORD Model: the multi-process version (it's time consuming, so multi-processing is necessary...)
    '''


    time_start = time.time()

    print 'read char list'
    char_list = read_reaction_time_csv_2500(reaction_time_csv)
    process_num = min(multiprocessing.cpu_count(), 20)


    #threshold = 0.14638  # use fix value for quick over-turn
    print 'Loading word2vec model...'
    w2v = Word2Vec.load(w2v_path)


    if len(w2v.vocab)>max_type_count:
        print 'pruning word2ve model to vocab size of', max_type_count, '.'
        prune_w2v(w2v, max_type_count)

    w2v_path='./tmp.word_model.w2v'
    w2v.save(w2v_path)




    #list_of_threshold = [sim_list[max(0,len(sim_list)/10000-1)], sim_list[max(0,len(sim_list)/1000-1)],\
    #                     sim_list[max(0,len(sim_list)/500-1)],sim_list[max(0,len(sim_list)/200-1)] ,sim_list[max(0,len(sim_list)/100-1)]]

    list_of_threshold=[0.05*i for i in range(21)]
   #
   # get a list of "interest-words", which contains at least one character in the character list
   #
    char2word_map={char:set() for char in char_list}
    words_of_interests = set()

    #word_list_ans = word_list[:min(max_type_count, len(word_list))]
    for word in w2v.vocab:
        for char in word:
            if char in char2word_map:
                char2word_map[char].add(word)
                words_of_interests.add(word)

    list_of_words= list(words_of_interests)
    print '\nNum of words of interests=', len(list_of_words)



    #
    # compute Ncount + ANS for these WORDS of interest
    #

    #for threshold in list_of_threshold:
        #threshold = sim_list[max(0,len(sim_list)/100-1)]

    batch_size = len(list_of_words)/process_num
    list_of_word_list =[ list_of_words[i*batch_size:(i+1)*batch_size]  for i in range(process_num)]
    if len(list_of_words)%process_num:
        list_of_word_list.append(list_of_words[process_num*batch_size:])
    arg_list = [(list_of_threshold,c_list, w2v_path) for c_list in list_of_word_list]

    print '\n'*3, 'Multiprocessing for computing predictors...'

    pool = multiprocessing.Pool(process_num)
    results = pool.map(compute_predictor_single_process, arg_list)
    pool.close()
    pool.join()

    word2nc_ans = {}
    for word2record in results:
        for word in word2record:
            word2nc_ans[word] = word2record[word]




    #
    #  convert word ncount, ans to  character ncount, ans...
    #
    # char2nc_ans: key:char value: sum_ncount, avg_ncount, median_ncount, sum_ans, avg_ans, median_ans
    char2nc_ans = word_stat_2_char_stat(word2nc_ans, char2word_map, list_of_threshold)

    print '\n'*3, '====  Results ====='
    for char in char2nc_ans:
        print char, char2nc_ans[char]

    word_model_char_record_pickle = './word.model.record.pickle'
    print '\n'*3, '>> Pickling the full list of neigbhors and the sims to ', word_model_char_record_pickle
    with open(word_model_char_record_pickle, 'wb') as f:
       pickle.dump(char2nc_ans,f)  #pickle the (ncount, nas) instead of the full neighbor list



    print '\n'*3, '>> Appending the new predictors to the old csv', path_csv, 'and writing to new csvs with prefix:', path_new_output
    append_predictor_to_csv(char2nc_ans, path_csv, path_new_output, list_of_threshold)

    time_diff = time.time() - time_start
    os.system ('echo "the ncount+ ans: word model task is done in '+ str(time_diff) + ' seconds " | mail -s word-model@suebi jianqiang.ma@uni-tuebingen.de')




#test_character_model()
#test_character_as_word_model()
#test_word_model()












