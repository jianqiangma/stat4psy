# -*- coding: utf-8 -*-

#
#merge_many_pickle.py: for picked freq counts of m sub-corpora, merge their results incrementally.
#
import pickle
import os, sys



pickle_num = 5


def get_prefix(file):
    return  '.'.join(file.split('.')[:-3])

def get_suffix(file):
    return '.'.join(file.split('.')[-3:])


def group_files(path):
    prefix2file={}
    abs_path = os.path.abspath(path)
    for file in os.listdir(abs_path):
        if os.path.isfile(abs_path+'/'+file) and file[-6:]=='pickle':
            if get_suffix(file) in {'word2freq.output.pickle', 'char2wordSet.output.pickle', 'char2freq.output.pickle',
                                    'char3gram2freq.output.pickle', 'char2docCount.output.pickle'}:

                file = abs_path+'/'+os.path.basename(file)
                prefix = get_prefix(file)
                prefix2file.setdefault(prefix, []).append(file)


    for value in prefix2file.values():
        if not len(value) == pickle_num:
            print '\nError! num of pickle files in each group should be', pickle_num, 'while actual num =', len(value)
            assert len(value) ==pickle_num

    #print prefix2file
    return prefix2file




def parse_suffix_get_id(file_suffix):

    tmp = file_suffix.split('page')[1].split('to')
    start_id = tmp[0]
    end_id = tmp[1].split('.')[0]

    return int(start_id), int(end_id)




def search_file(path, list_of_file_id_scope):

    path = os.path.abspath(path)

    prefix2file = group_files(path)

    selected_pickle_group=[]

    for file_id_scope in list_of_file_id_scope:

        for file_group_prefix in prefix2file:
            start_id, end_id = parse_suffix_get_id(file_group_prefix)

            #print 'start/end id=', start_id, end_id
            #print 'targte star/end', file_id_scope[0], file_id_scope[1]

            if start_id>= file_id_scope[0] and end_id<=file_id_scope[1]:
                selected_pickle_group.append(file_group_prefix)

    print '\nSelected file group:'
    for s in selected_pickle_group:
        print s

    print 'total count=', len(selected_pickle_group)

    return selected_pickle_group, prefix2file  #Do NOT need to return prefix2file, as the actual file suffix have already been retrieved and kept in selected_pickle_gropu_list




def merge_two_maps(map1, map2):

    keys= set(map1.keys()).union(set(map2.keys()))
    print 'num of keys in union, map1, map2:',len(keys), len(map1.keys()), len(map2.keys())

    type_flag = type(map1.values()[0])

    assert type(map2.values()[0]) is type_flag


    print 'Merging two maps'
    new_map={}

    for key in keys:
        flag1, flag2= key in map1, key in map2
        #print flag1, flag2

        if flag1 and flag2:

            if type_flag is set:
                new_map[key] = map1[key].union(map2[key])

            elif type_flag is int:
                new_map[key] = map1[key]+ map2[key]

            else:
                print 'Error! unsupported type',type_flag, 'of the map values is in neither set (in: merge_pickle.merge())'
                return

        else:
            if  flag1:
                new_map[key] = map1[key]

            elif flag2:
                new_map[key] = map2[key]

            else:
                print 'Error! key',key, 'is in neither set (in: merge_pickle.merge())'
                return
    print 'merging done.'
    return new_map



def incremental_merge_pickles(selected_pickle_group_list, prefix2file):


    if len(selected_pickle_group_list) == 1:
        print '\n==>only one pickle group satisfy the file-id span, do not need to merge'
        print 'the prefix of this pickle group is ', selected_pickle_group_list[0]

    else:
        suffix2map={}
        file_group1, file_group2 = prefix2file[selected_pickle_group_list[0]], prefix2file[selected_pickle_group_list[1]]
        file_group1.sort()
        file_group2.sort()


        list_of_pair = zip(file_group1, file_group2)


        for file1, file2 in list_of_pair:
            suffix = get_suffix(file1)
            assert suffix == get_suffix(file2)
            print '\nLoading map from pikle file ', file1, ' &', file2
            map1 = pickle.load(open(file1, 'rb'))
            map2 = pickle.load(open(file2, 'rb'))

            new_map = merge_two_maps(map1, map2)
            suffix2map[suffix] = new_map

        print '\n-------->   @suffix2map size:',len(suffix2map)
        for s in suffix2map:
            print s, len(suffix2map[s].keys())

        for file_prefix in selected_pickle_group_list[2:]:
            print '\n\nmerging with file group', file_prefix, '...'

            file_group = prefix2file[file_prefix]

            for file in file_group:
                print '\n===> current pickle file being merged:', file, '...'
                suffix = '.'.join(file.split('.')[-3:])

                print '> retrieve current map'
                old_map = suffix2map[suffix]
                #print 'type/size of retrieved old map',type(old_map), len(old_map)

                print '>> loading additional map..'
                additive_map = pickle.load(open(file, 'rb'))
                #print 'type/size of loaded additional map',type(additive_map), len(additive_map)

                print '>>> merge...'
                new_map = merge_two_maps(old_map, additive_map)

                print 'Update the map with merged map', suffix
                suffix2map[suffix] = new_map
                print 'done for current pickle'

        return suffix2map


def write_dictionary(suffix2map, prefix):
    for suffix in suffix2map:
        print 'saving the merged ', suffix, 'to file',prefix+suffix, '...'
        pickle.dump(suffix2map[suffix], open(prefix+suffix, 'wb'))

    print '\nWriting done!'



#path = '../working_data/'
path = '/home/jma/sogouT.stat'
prefix=path+'/merged_pickle/s.merge.groupB.1t29.50t99.156t288.'
selected_pickle_group_list, prefix2file  = search_file(path, [[1, 29],[50,99], [156,288]])
#selected_pickle_group_list, prefix2file  = search_file(path, [[30, 49],[100,155]])
#selected_pickle_group_list, prefix2file = search_file(path, [[1,2],[3,6]])
suffix2map = incremental_merge_pickles (selected_pickle_group_list, prefix2file)
write_dictionary(suffix2map, prefix)

os.system('echo "merge-pickle group B is done" | mail -s m_pickle_B jianqiang.ma@uni-tuebingen.de')


