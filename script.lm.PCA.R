#
#script_ans_w2v_hidex.R
#


setwd("~/Dropbox/Code/QuantLing/Cyrus_reaction_time/src")
source('enum4.R')  #only differs with enum3.R in that it only gives top-1 result of top-3



pca_scale_lm <- function(csv_path){
  cat ('\n\n\n#####Result for ', csv_path)
  #print ('###Result for data'+csv_path)
  #explicit pre-processing (same as preprocessing funciton in enum2.R)
  rdata = read.csv(csv_path)
  rdata = subset(rdata, rdata$Acc>0)
  predictors = data.frame(rdata$Z.RT., log(rdata$char_prob), log(rdata$char_word_prob), log(rdata$char_doc_prob), rdata$char_ent, rdata$head_ent, rdata$tail_ent, rdata$trigram_ent, log(rdata$trigram_context), log(rdata$ncount), rdata$ans)
  names(predictors) = c("zrt","char_prob","word_prob","doc_prob","char_ent","head_ent","tail_ent","trigram_ent", "trigram_countext","ncount","ans")
  predictors[is.na(predictors)] <- -1.0
  
  s_pred = data.frame(cbind(predictors[,1],scale(predictors[,2:11])))
  names(s_pred)[1]='zrt'
  summary(s_pred)
  
  
  x=lm_by_enum_without_preproc(s_pred)
  
  
  
  
  #
  # do PCA, and run the LM with full PCA components
  #
  pr = prcomp(s_pred[,2:11])
  print (pr)
  
  #barplot(pr$sdev/pr$sdev[1]) #relative 
  
  m_pred = as.matrix(s_pred[,2:11])
  
  transformed_s_pred  = data.frame(cbind(s_pred[,1],m_pred%*%pr$rotation))
  names(transformed_s_pred)[1]='zrt'
  y = lm_by_enum_without_preproc(transformed_s_pred)
  
  return 

}




#csv_path="../input_data/rt.stat.sogou.all.new.2.5"
#csv_path="../input_data/rt.stat.sogou.all.hidex.csv"
#csv_path="../input_data/rt.stat.sogou.all.my.hidex.csv"
args<-commandArgs(TRUE)
pca_scale_lm (args)
#csv_path="../input_data/hidex.char.sogouT.csv"
#pca_scale_lm(csv_path)