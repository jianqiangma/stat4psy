#single_multi_ratio.py: compute the ratio of character occurs as a single-char word and appears in the multi-char word
#-*- coding: utf-8 -*-

import cPickle as pickle
import codecs

def read_char_list(char_path):

	with codecs.open(char_path, 'ru', 'utf-8') as f:
		char_set =set([token for line in f for token in line.split()])
		print '\nsize of character size=', len(char_set)
		print 'example characters:', ' '.join(list(char_set)[:min(3, len(char_set))])

	return char_set

def compute_ratio(char_cvs, char2words_pickle, word2freq_pickle, output):
	char_set = read_char_list(char_cvs)

	print '\nLoading char2word from ', char2words_pickle
	char2word = pickle.load(open(char2words_pickle, 'rb'))
	target_char2word = {char:char2word[char] for char in char_set}
	#char2single = {char:0 for char in char_set}
	#char2multi = {char:0 for char in char_set}
	char2single, char2multi = {}, {}

	print "\nLoading word2freq_pickle"
	word2freq = pickle.load(open(word2freq_pickle, 'rb'))

	print '\nComputing single/multi ratio for characters'
	for char in target_char2word:
		for word in target_char2word[char]:
			if word in word2freq:
				freq = int(word2freq[word])

				if len(word)==1 :
					char2single[char] = char2single.get(char, 0) + freq
				elif len(word)>1 :
					char2multi[char] = char2multi.get(char, 0) + freq

	char2ratio_list = [(char, char2single[char]/float(char2single[char]+char2multi[char])) for char in char_set]
	char2ratio_list.sort(key=lambda x:x[-1], reverse=True)

	print '\nWriting results to ', output
	with codecs.open(output, 'w', 'utf-8') as f:
		for char, ratio in char2ratio_list:
			f.write(char+" "+str(ratio)+"\n")
	print 'done!'
def test():
	'''
	for running @suebi: /home/jma/sogouT.stat/z_merged_pickle, where all data exists
	'''
	char_cvs, char2words_pickle, word2freq_pickle, output =\
	"char.2500", "sogou.all.stat.char2wordSet.output.pickle", "sogou.all.stat.word2freq.output.pickle", "single_multi_ratio.txt"
	compute_ratio(char_cvs, char2words_pickle, word2freq_pickle, output)


test()







