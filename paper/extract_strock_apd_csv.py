#extract_stroke_append_to_csv.py extract stroke from 
#-*- coding: utf-8 -*-
import codecs

def extract_stroke(stroke_file):
	char2stroke={}
	with codecs.open(stroke_file,'rU','utf-8') as f:
		lines = f.readlines()[1:]
		for l in lines:
			tokens = l.strip().split()
			char2stroke[tokens[2]] = tokens[4]
			char2stroke[tokens[3]] = tokens[5]
	return char2stroke


def append_predictor_to_csv(char2stroke, path_csv, path_new_output):
	no_stroke_counter = 0
	with codecs.open(path_csv,'rU','utf-8') as f:
		lines = f.readlines()
		head_str = lines[0]
		records = lines[1:]
	new_head_str = head_str.strip()+', stroke\n'
	with codecs.open(path_new_output, 'w','utf-8') as f:
		f.write(new_head_str)
		for line in records:
			char = line.split(',')[0]
			if char in char2stroke:
				new_line = line.strip() + ','+char2stroke[line.split(',')[0]]+'\n'
			else:
				new_line = line.strip() +', 8\n'  

				no_stroke_counter += 1
				print '\n>> char', char, 'has no stroke Record!'
			f.write(new_line)
	
	print '\nNum of chars without stroke=', no_stroke_counter, 'make their strkoe count == 8'
	print '\nAppending the stroke predictors to csv, result@', path_new_output	





stroke_file="strokeRaw.csv"
old_csv ="rt.data.hidex.csv"
new_csv ="final.rt.hidex.stroke.csv"

char2stroke = extract_stroke(stroke_file)
append_predictor_to_csv(char2stroke, old_csv, new_csv)