#extract_ratio_apd_csv.py
#-*- coding: utf-8 -*-
import codecs

def extract_ratio(ratio_file):
	char2ratio={}
	with codecs.open(ratio_file,'rU','utf-8') as f:
		for l in f:
			tokens = l.strip().split()
			char2ratio[tokens[0]] = tokens[1]
	return char2ratio


def append_predictor_to_csv(char2ratio, path_csv, path_new_output):
	ratio_list=map(float, char2ratio.values())
	median = str(ratio_list[len(ratio_list)/2])
	no_ratio_counter = 0
	with codecs.open(path_csv,'rU','utf-8') as f:
		lines = f.readlines()
		head_str = lines[0]
		records = lines[1:]
	new_head_str = head_str.strip()+', ratio\n'
	
	with codecs.open(path_new_output, 'w','utf-8') as f:
		f.write(new_head_str)
		for line in records:
			char = line.strip().split(',')[0]
			if char in char2ratio:
				new_line = line.strip() + ','+char2ratio[char]+'\n'
			else:
				new_line = line.strip() +','+median+'\n'  
				no_ratio_counter += 1
				print '\n>> char', char, 'has no ratio Record!'
			f.write(new_line)
	
	if no_ratio_counter>0:
		print '\nNum of chars without ratio=', no_ratio_counter, 'make their ratio count == ',median
	print '\nAppending the stroke predictors to csv, result@', path_new_output	





ratio_file="single_multi_ratio.txt"
old_csv ="final.rt.hidex.stroke.csv"
new_csv = "wrapup.rt.hidex.stroke.ratio.csv"

char2ratio = extract_ratio(ratio_file)
append_predictor_to_csv(char2ratio, old_csv, new_csv)