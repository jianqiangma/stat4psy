#
# merge_pickle
#

import cPickle as pickle
import os.path, sys


def detect_pickle(path):
    w2f_path = path+'.word2freq.output.pickle'
    c2ws_path = path+'.char2wordSet.output.pickle'
    c2f_path = path +'.char2freq.output.pickle'
    c3gram_path = path+'.char3gram2freq.output.pickle'
    c2doc_path = path + '.char2docCount.output.pickle'

    pickle_path_list = [w2f_path, c2ws_path, c2f_path, c3gram_path, c2doc_path]


    flag = True
    for file_name in pickle_path_list:
        if not os.path.isfile(file_name):
            print file_name, ' does not exist'
            flag = False


    if flag:
        print 'all pickle files successfully detected for ', path
        return pickle_path_list
    else:
        return None




def merge(list_of_pair):
    for file1, file2 in list_of_pair:
        assert file1.split('.')[-3:] == file2.split('.')[-3:]

        directory = os.path.dirname(file1)
        assert directory == os.path.dirname(file2)

        
        if directory:
            new_prefix = directory+'/'+'.'.join(os.path.basename(file1).split('.')[:-3])+'_'+'.'.join(os.path.basename(file2).split('.')[:-3])
        else:
            new_prefix='.'.join(os.path.basename(file1).split('.')[:-3])+'_'+'.'.join(os.path.basename(file2).split('.')[:-3])

        #print '\nfile1, file2=', file1, file2
        #print '\n*** new_prefix', new_prefix

        

        print '\nLoading map from pikle file ', file1, ' &'
        print file2
        map1 = pickle.load(open(file1, 'rb'))
        map2 = pickle.load(open(file2, 'rb'))

        keys= set(map1.keys()).union(set(map2.keys()))
        print 'num of keys in union, map1, map2:',len(keys), len(map1.keys()), len(map2.keys())

        type_flag = type(map1.values()[0])

        assert type(map2.values()[0]) is type_flag


        print 'Merging two maps'
        new_map={}
            
       
        if True:

            for key in keys:
                flag1, flag2= key in map1, key in map2
                #print flag1, flag2

                if flag1 and flag2:

                    if type_flag is set:
                        new_map[key] = map1[key].union(map2[key])

                    elif type_flag is int:
                        new_map[key] = map1[key]+ map2[key]

                    else:
                        print 'Error! unsupported type',type_flag, 'of the map values is in neither set (in: merge_pickle.merge())'
                        return

                else:
                    if  flag1:
                        new_map[key] = map1[key]

                    elif flag2:
                        new_map[key] = map2[key]

                    else:
                        print 'Error! key',key, 'is in neither set (in: merge_pickle.merge())'
                        return



        new_path = new_prefix+'.'+'.'.join(file1.split('.')[-3:])
        print 'writing the result to path:', new_path


     
        with open(new_path, 'wb') as f:
           pickle.dump(new_map, f)

        print 'done!'

                    



def merge_pickle(name1, name2):
    pickle_path_list1 = detect_pickle(name1)
    pickle_path_list2 = detect_pickle(name2)

    if pickle_path_list1 and pickle_path_list2:

        list_of_pair = zip(pickle_path_list1, pickle_path_list2)
        merge(list_of_pair)

    else:
        print 'error! merge can not be conducted! as one of the pickle file group  does not confrom the format required for the 5 pickles... '
        print "which are: '.word2freq.output.pickle', '.char2wordSet.output.pickle',\
                    '.char2freq.output.pickle', '.char3gram2freq.output.pickle', '.char2docCount.output.pickle' "

        if not pickle_path_list1 :
            print 'pickle group whose prefix is', name1, 'is problematic'

        if not pickle_path_list2:
            print 'pickle group whose prefix is', name2, 'is problematic'

        


if __name__=='__main__':
    merge_pickle(sys.argv[1], sys.argv[2])
    
            

                    
                
                
        


