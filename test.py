#
# Final module that works on real data
#
import codecs, pickle, multiprocessing, sys
#from sys import exit


#
# function to deal with a single document, same as in [6]
#
def gen_stat_for_a_doc(char_set_n_doc):  
    
    char_set, doc = char_set_n_doc
    char2freq, char2wordSet = dict(), dict()
    
    #char_freq={char:0 for char in char_set}
    #char2word={char:set() for char in char_set}
    
    for line in doc:
        #print line
        word_seq=line.split()
        
        for word in word_seq:
            #print 'word=', word
            for char in word:
                if char in char_set:  
                    
                    char2freq[char] = char2freq.get(char, 0) + 1  
                    char2wordSet.setdefault(char, set()).add(word)
                    #print char, char2freq[char], u''.join(char2wordSet[char])
                    
        
    #char_in_doc = set(char2freq.keys())   #---> do not explicitly need this, as it can always get from char2freq.keys()
    
    return char2freq, char2wordSet


def update_corpus_level_stat(result_list, char2freq_corpus, char2wordSet_corpus, char2docCount_corpus):
    for char2freq, char2wordSet in result_list:
        for char in char2freq:
            char2freq_corpus[char] += char2freq[char]
            char2wordSet_corpus[char].update(char2wordSet[char])
            char2docCount_corpus[char] += 1
    
    result_list=[]

    
#test_multiprocessing of stat collection
def function_to_process_corpus(path_to_corpus, char_set, end_of_doc_sign, num_process, batch_size):
    
    pickle_intermediate_result = False
    
    char2freq_corpus={char:0 for char in char_set}
    char2wordSet_corpus={char:set() for char in char_set}
    char2docCount_corpus={char:0 for char in char_set}
    
    
    line_count, cum_line_count, batch_count = 0, 0, 0
    
    line_buffer=[]
    docs=[]
    
    with codecs.open(path_to_corpus,'rU', 'utf-8') as f:
        print '\nReading segmented corpus from', path_to_corpus, '...'
        
        for line in f:
            
            line_count += 1
            line_buffer.append(line)
            
            #
            #triger doc collection once end_of_doc sign is seen
            #
            if line.split()[0]==end_of_doc_sign:
                
                docs.append(line_buffer)
                line_buffer=[]
                
                
                #
                # batch process multiple documents, when there are large enough docs cummulated
                #
                if line_count> batch_size: #simulate creteria for staring multi-processing
                    
                    cum_line_count += line_count
                    line_count = 0
                    batch_count += 1
                   
                    print cum_line_count, 'lines'
                    charset_doc_tuples=[(char_set, doc) for doc in docs]
                    docs=[]
                    
                    print '\nBatch process, running multiprocessing...\n'
                    pool = multiprocessing.Pool(num_process)
                    
                    try:
                        result_list = pool.map_async(gen_stat_for_a_doc, charset_doc_tuples).get(9999999)    
                        #purpose of using map_async and a timeout parameter ===> to make keyboard interuption feasible,
                        #see http://stackoverflow.com/questions/1408356/keyboard-interrupts-with-pythons-multiprocessing-pool
                    
                    except KeyboardInterrupt:
                        pool.terminate()
                        print "You cancelled the program!"
                        sys.exit(1)
                    
                    pool.close()
                    pool.join()
                    
                    if pickle_intermediate_result:
                    
                        print '\nPickleing intermediate results...'
                        pickle_path=path_to_corpus+'.tmp_result.'+str(batch_count)+'.pickle'
                        with open(pickle_path, 'wb') as f:
                            pickle.dump(result_list, f)
                    
                    
                    #
                    #update result to corpus_level statistics
                    #
                    print '\nUpdating global statistics'
                    update_corpus_level_stat(result_list, char2freq_corpus, char2wordSet_corpus, char2docCount_corpus)

            
            
            # NOT a end_of_doc line, i.e. ordinary lines
            #else: 
                #pass
                
    if line_buffer:
        print '(diagnostic info:left-over lines)'
        docs.append(line_buffer)
    
    if docs:
        print '(diagnostic info: left-over docs will be processed, num of such docs=',len(docs),')'
        
        charset_doc_tuples=[(char_set, doc) for doc in docs]
        
        pool=multiprocessing.Pool(num_process)
        result_list=pool.map(gen_stat_for_a_doc, charset_doc_tuples)
        pool.close()
        pool.join()
        
        if pickle_intermediate_result:
            
            print '\nPickleing intermediate results...'
            pickle_path=path_to_corpus+'.tmp_result.'+str(batch_count)+'.pickle'
            with open(pickle_path, 'wb') as f:
                pickle.dump(result_list, f)
        
        
        update_corpus_level_stat(result_list, char2freq_corpus, char2wordSet_corpus, char2docCount_corpus)
        
    
    print '\n>>done!'
    return char2freq_corpus, char2wordSet_corpus, char2docCount_corpus
                

#
# new function to write the result...
#
def pickle_statistics(char2freq_corpus, char2wordSet_corpus, char2docCount_corpus, path_to_corpus):
    
    output1, output2, output3 = path_to_corpus+'.char2freq.output.pickle',  path_to_corpus+'.char2wordSet.output.pickle',\
    path_to_corpus+'.char2docCount.output.pickle'
    
    print '\n\n>> Writing results for current corpus to files', output1, output2, output3
    
    with open(output1, 'wb') as f:
        pickle.dump(char2freq_corpus, f)
    
    with open(output2, 'wb') as f:
        pickle.dump(char2wordSet_corpus, f)
    
    with open(output3, 'wb') as f:
        pickle.dump(char2docCount_corpus, f)
    
    
    
    
#
# main function
#
def main():
    
    char_cvs, path_to_corpus  = '../input_data/small.csv', '../working_data/sample.seg'
    
    end_of_doc_sign = 'ZZZENDOFDOCUMENTZZZ'
    num_process = 4
    batch_size = 200000 #process every 10k lines
    
    with codecs.open(char_cvs, 'rU','utf-8') as f:
        char_list=[line.split(',')[1] for line in f.readlines()[1:]]
    char_set=set(char_list)
    
    char2freq_corpus, char2wordSet_corpus, char2docCount_corpus \
    = function_to_process_corpus(path_to_corpus, char_set, end_of_doc_sign, num_process, batch_size)
    
    pickle_statistics(char2freq_corpus, char2wordSet_corpus, char2docCount_corpus, path_to_corpus)

main()

