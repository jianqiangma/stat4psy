# -*- coding: utf-8 -*-
import codecs, sys

def conv628(path):
    with codecs.open(path,'rU','utf-8') as f:
        corpus = []
        
        for i, line in enumerate(f):
            token_list = line.split(',')
            new_token_list = [ u'0.0' if token == u'.' else token for token in token_list  ]
            if token_list == new_token_list:
               corpus.append(line)
            else:
                print 'fix line ', i, 'invalid format:', line
                corpus.append(u','.join(new_token_list)+'\n')
                
    path_out = path+'.refined'

    with codecs.open(path_out,'w', 'utf-8') as f:
        for line in corpus:
            f.write(line)

    print 'done!'


if __name__=='__main__':
    print '\nconv reaction time data to substitute . with 0.0 to make it comply with csv format'
    print '@Arg: input_path  output will be input_path.refined'
    conv628(sys.argv[1])
