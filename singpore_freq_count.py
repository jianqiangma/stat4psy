# -*- coding: utf-8 -*-
import codecs, multiprocessing, os, sys, time

def read_singapore_list(f_path):
    with codecs.open(f_path, 'rU','utf-8') as f:
        word_set=set([line.strip().split(',')[-1] for line in f.readlines()[1:]])
        return word_set

def read_singapore_list_nonwords(f_path):
    with codecs.open(f_path, 'rU','utf-8') as f:
        word_set=set()
        for line in f.readlines()[1:]:
            tokens= line.strip().split(',')[4:]
            #print ' '.join([ tokens[3*i] for i in range(6)])
            for word in [ tokens[3*i] for i in range(6)]:
                if len(word)>0:  #centrain empty lines are there...
                    word_set.add(word)
        #word_set=set([line.strip().split(',')[-1] for line in f.readlines()[1:]])
        return word_set


def compute_freq(arg_tuple):
    word_set, file_path = arg_tuple
    word2freq={word:0 for word in word_set}
    with codecs.open(file_path, 'rU','utf-8') as f:
        for line in f:
            for word in line.split():
                if word in word2freq:
                    word2freq[word] += 1

    return word2freq


def parallel_freq_count(path_word_list, file_directory, path_out):
    if "nonword" in path_word_list:
        word_set = read_singapore_list_nonwords(path_word_list)
    else:
        word_set = read_singapore_list(path_word_list)

    if os.path.isdir(file_directory):
        file_list =[os.path.abspath(file_directory)+'/'+p for p in os.listdir(os.path.abspath(file_directory)) if os.path.isfile(os.path.abspath(file_directory)+'/'+p)]

        print 'the file list:', '\n'.join(file_list)

        arg_list = [(word_set, p) for p in file_list]

        print 'len arg_list=', len(arg_list)

        num_process = min(multiprocessing.cpu_count(), len(arg_list))

        pool = multiprocessing.Pool(num_process)

        result_list = pool.map(compute_freq, arg_list)

        pool.close()
        pool.join()

        word2freq={word:0 for word in word_set}

        for w2f in result_list:
            for word in w2f:
                freq=w2f[word]
                word2freq[word] += freq


        print '\nResult====='
        for word in word2freq:
            print word, word2freq[word]


        with codecs.open(path_out, 'w','utf-8') as f:
            for word in word2freq:
                f.write(word+', '+str(word2freq[word])+'\n')

        print 'done! the result is in ', path_out


    else:
        print 'Error! the given file directory is NOT a directory'





if __name__=='__main__':
    print '\nRunning singapore frequency count'
    print '\n@Arg 1: path to the sigapore word list, 2.directory to the file that contain corpus/subcorpora, 3.path to result'
    x=time.time()
    #if len(sys.argv)>4:

    parallel_freq_count(sys.argv[1], sys.argv[2], sys.argv[3])
    os.system ('echo "the singapore task is done in '+ str(time.time()-x) + ' seconds " | mail -s singapore@suebi jianqiang.ma@uni-tuebingen.de')