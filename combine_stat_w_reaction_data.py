#-*- coding: utf-8 -*-

import codecs, sys

def read_reaction_time_csv_2500(reaction_time_csv):
    '''
    read the csv file tha contains the raction time data of the 2500 characters
    '''
    print '\nread the originalreaction time data with minor fix or . errors'
    with codecs.open(reaction_time_csv, 'rU', 'utf-8') as f:

        if '2500' in reaction_time_csv:
            char_info_list = [line.strip().split(',') for line in f.readlines()]
            head_str = char_info_list[0]
            char_info_with_error = char_info_list[1:]

            reaction_time_records = []
            for token_list in char_info_with_error:
                new_token_list = [ u'0.0' if token == u'.' else token for token in token_list]
                reaction_time_records.append(new_token_list)

        else:
            print '\Error! The input_data is not the recognized file!', reaction_time_csv


    return head_str, reaction_time_records


def read_stat_csv(stat_csv):
    print '\nread the statistics cvs generated from corpus'
    with codecs.open(stat_csv, 'rU', 'utf-8') as f:
        char_info_list = [line.strip().split(',') for line in f.readlines()]
        head_str =char_info_list[0][1:]  # do not include the first column in stat_head, which is "char", 

    char2stat = {}
    for record in char_info_list[1:]:
        char2stat[record[0]] = record[1:]

    return head_str, char2stat

def combine_stat_w_reaction_data(reaction_csv, stat_csv, r_stat_csv):

    print '\nCombining original reaction time data with the statistics '
    r_time_head, reaction_time_records = read_reaction_time_csv_2500(reaction_csv)
    stat_head, char2stat = read_stat_csv(stat_csv)

    print '\ncombining the two and write to file', r_stat_csv

    with codecs.open(r_stat_csv,'w','utf-8') as f:
        f.write(','.join(r_time_head+stat_head)+'\n')
        #f.write("Character,Acc,Ntrials,RT,SE,SD,Z(RT), char_prob, char_word_prob, char_doc_prob, char_ent, head_ent, tail_ent, trigram_ent, trigram_context\n")

        for record in reaction_time_records:
            f.write(','.join(record + char2stat[record[0]])+'\n')

    print 'done'



if __name__=='__main__':
    print '\nread the statistics cvs generated from corpus'
    print '\n@Arg: 1. reaction time csv  2. csv of char statistics from corpus   3.output csv file '
    combine_stat_w_reaction_data(sys.argv[1], sys.argv[2], sys.argv[3])



