# -*- coding: utf-8 -*-
import codecs, sys

def read_hidex_result(path_hidex):
    with codecs.open(path_hidex, 'rU','utf-8') as f:
        char2record ={}
        for line in f.readlines()[1:]:
            assert len(line.split()) == 5
            tokens = line.strip().split()
            #print ' $ '.join(tokens)
            #print '#', tokens[3], tokens[2]
            char2record[tokens[0]] = (int(tokens[3])+1, float(tokens[2]))

    return char2record



def append_predictor_to_csv(char2nc_ans, path_csv, path_new_output):

    with codecs.open(path_csv,'rU','utf-8') as f:
        lines = f.readlines()
        head_str = lines[0]
        records = lines[1:]


    new_head_str = head_str.strip()+', ncount, ans\n'

    path_new_output2=path_new_output
    with codecs.open(path_new_output2, 'w','utf-8') as f:
        f.write(new_head_str)
        for line in records:
            char = line.split(',')[0]
            if char in char2nc_ans:
                new_line = line.strip() + ','+','.join(map(str, char2nc_ans[line.split(',')[0]]))+'\n'
            else:
                new_line = line.strip() +', 1, 0.0\n'
                print '\n>> char', char, 'has no Hidex Record!'
            f.write(new_line)

    print '\nAppending the Ncount and ANS predictors to csv, result@', path_new_output2



def main(path_csv, path_hidex, path_new_output):
    char2record=read_hidex_result(path_hidex)
    append_predictor_to_csv(char2record, path_csv, path_new_output)


if __name__=='__main__':
    print '\nAppending Hidex results of Ncount and ANS to the original CSV of 2500 characters reaction time data'
    print '\n@arg 1.path_original_csv  2.path_hidex_ans.txt file   3.path new output'
    main(sys.argv[1], sys.argv[2], sys.argv[3])

    print '\nJobs done. Program exit...'
    print '''
    WARNING: If N/A error happens in the later state of linear modeling, go to read_hidex_result() function and change
    char2record[tokens[0]] = (int(tokens[3]), float(tokens[2]))  ==> char2record[tokens[0]] = (int(tokens[3])+1, float(tokens[2]))
    (simple add one smoothing, as we will use log(ncount) instead of raw ncount)
    '''

#
#
# If N/A error happens in the later state of linear modeling, go to read_hidex_result() and change
#       char2record[tokens[0]] = (int(tokens[3]), float(tokens[2]))  ==> char2record[tokens[0]] = (int(tokens[3])+1, float(tokens[2]))
#
#