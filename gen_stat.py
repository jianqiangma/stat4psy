__author__ = 'jma'
#-*- coding: utf-8 -*-
import codecs, multiprocessing, sys, time, math, os
import cPickle as pickle

import large_corpora_stat_gen_for_qling
from compute_entropy import derive_auxilary_data, do_char_head_tail_entropy, do_trigram_entropy, write_stat, float2str




#
# write character and their stats, without outputing original re-action time
#
def write_stat(char_set, char_prob, char_word_prob, char_doc_prob, char2entropy, new_head2ent, new_tail2ent, trigram_ent, trigram_count, output):
    print 'len char2entropy, head2ent, tail2ent, trigram_context=', len(char2entropy), len(new_head2ent), len(new_tail2ent)
    print '\nResult:', u"char, char_prob, char_word_prob, char_doc_prob, char_ent, head_ent, tail_ent, trigram_ent, trigram_context"+u'\n'

    with codecs.open(output, 'w','utf-8') as f:

        f.write( u"char, char_prob, char_word_prob, char_doc_prob, char_ent, head_ent, tail_ent, trigram_ent, trigram_context"+u'\n')
        char_list = list(char_set)
        char_list.sort()

        for i, char in enumerate(char_list):

            cvs_line_str = u",".join(map(float2str,[char_prob[char], char_word_prob[char], char_doc_prob[char],\
                                                    char2entropy[char], new_head2ent[char], new_tail2ent[char], trigram_ent[char],trigram_count[char]]))
            f.write(char+u","+cvs_line_str+ u"\n")
            print  char+u","+cvs_line_str





def main(char_cvs, path_to_corpus, end_of_doc_sign, output, pickle_flag):

    print '\n\n========= Phase I computing frequencies of characters, words and ngrams of the interest======='

    x=time.time()

    num_process = int(min(12, multiprocessing.cpu_count()))
    batch_size = 9000

    #read cvs file that contains reaction time data
    #char_info_list, head_str, \

    #read char list that contains all the target character
    char_set = large_corpora_stat_gen_for_qling.read_char_list(char_cvs)

    # collect the frequency count from the large corpus

    char2freq, char2_wordset,  char2_doccount, word2freq, char3gram2freq \
        = large_corpora_stat_gen_for_qling.compute_frequency_counts(path_to_corpus, char_set, end_of_doc_sign, num_process, batch_size)

    # compute relative probability using frequency count collected
    char_freq_smoothed, char_word_freq_smoothed,  char_doc_freq_smoothed  = large_corpora_stat_gen_for_qling.compute_smoothed_freq(char2freq, char2_wordset, char2_doccount, char_set)


    #pickling frequency counts for other program's usage
    if pickle_flag:
        large_corpora_stat_gen_for_qling.pickle_statistics(char2freq, char2_wordset, char2_doccount, word2freq, char3gram2freq, path_to_corpus, char_cvs)



    print '\n\n========= Phase II computing vairous entropy values======='

    print 'gen auxiliary data'
    bichar_word_freq_list, char2_twocharword = derive_auxilary_data(char2_wordset, word2freq) #auxiliary data

    print '\nCompute char, head, and tail entropy...'
    char2entropy,   new_head2ent, new_tail2ent = do_char_head_tail_entropy (bichar_word_freq_list, char2_twocharword, char2freq, char_set, word2freq)

    print '\nCompute character trigram entropy...'
    trigram_ent, trigram_count = do_trigram_entropy(char3gram2freq, char_set)

    print '\n\n>> Computation done! Writing output to', output, '...'

    #old writing function no longer used ....
    #write_output(char_info_list, head_str, char_freq_smoothed, char_word_freq_smoothed, char_doc_freq_smoothed, char2entropy, new_head2ent, new_tail2ent, trigram_ent, trigram_count, output)

    #only write the statistics collect for characters, without original re-action time data (do that merge later...)
    write_stat(char_set, char_freq_smoothed, char_word_freq_smoothed, char_doc_freq_smoothed, char2entropy, new_head2ent, new_tail2ent, trigram_ent, trigram_count, output)

    print '\n\nJob finished in ', time.time()-x, ' seconds!'

    os.system ('echo "gen_stat is done " | mail -s GenStatDone jianqiang.ma@uni-tuebingen.de')






def test():
   char_cvs='../working_data/2500.Chinese.Lexicon.Project.csv'
   path_to_corpus='../working_data/small.seg'
   end_of_doc_sign= 'ZZZENDOFDOCUMENTZZZ'
   output='../working_data/new.tmp.txt'
   main(char_cvs, path_to_corpus, end_of_doc_sign, output, False)

#test()

if __name__=='__main__':

    print '\n\n>>Gen stastistics from large scale corpora for reaction time experiment...'
    print '\nArg: 1.target_char.cvs, 2.corpus, 3. end_of_doc_sign, 4.output, (and optional) 5.whether_to_pickel_freq_count (true by default)'


    if len(sys.argv)<6:
        pickle_flag = True

    else:
        if str(sys.argv[5]).lower()=='false':
            pickle_flag = False
        else:
            pickle_flag = True

    main(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], pickle_flag)


